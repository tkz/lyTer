package lyter

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/tkz/lyTer/portfolio"
)

var (
	sendEmails                                = true
	emailSubjects, emailAlerts, emailMessages map[string]string
)

/*
 * Alert Funcs
 * ===========
 */

func alertAppUp() {
	//log
	if checkLogLevel(0) {
		log.Printf("Up!")
	}

	//send startup alert email w/account summary
	if sendEmails {
		sendAppUpEmail()
	}
}

func alertAppFly() {
	//log
	if checkLogLevel(0) {
		log.Printf("Fly ticker started!\n\n%s\n", portfolio.ActiveStrategy.String())
	}

	//send startup alert email w/account summary
	if sendEmails {
		sendAppFlyEmail()
	}
}

func alertAppFlyComplete() {
	//log
	if checkLogLevel(0) {
		log.Printf("Fly Complete!")
	}

	//send startup alert email w/account summary
	if sendEmails {
		sendAppFlyCompleteEmail()
	}
}

func alertAppUpComplete() {
	//log
	if checkLogLevel(0) {
		log.Printf("Up Complete!")
	}

	//send startup alert email w/account summary
	if sendEmails {
		sendAppUpCompleteEmail()
	}
}

func alertAppTradeReport() {
	//log
	if checkLogLevel(1) {
		log.Printf("generating trade report..")
	}

	tradeReport := generateTradeReport()

	//log
	if checkLogLevel(1) {
		log.Printf("done!")
	}

	//send startup alert email w/account summary
	if sendEmails {
		//log
		if checkLogLevel(1) {
			log.Printf("emailing trade report..")
		}

		sendAppTradeReportEmail(tradeReport)

		//log
		if checkLogLevel(1) {
			log.Printf("done!")
		}
	}
}

/*
 * Email Funcs
 * ===========
 */
func sendAppUpEmail() {
	//print account summary at startup
	acct, err := getAccount()
	checkPanic(err, fmt.Sprint(err))

	//send startup alert email with account info as data
	dataHTML := fmt.Sprintf(`<div style="background-color: ffffff;"><br/>&emsp;<strong>Account Summary:</strong><br/></div><div style="padding-left:25px;"><small><pre><code>%s</code></pre></small></div>`, acct.String())
	sendEmail(emailSubjects["app"], "greenAlertMessageData",
		//alert
		emailAlerts["appStart"],

		//message
		emailMessages["appStart"],

		//data
		dataHTML,
	)
}

func sendAppFlyEmail() {
	//print account summary at startup
	acct, err := getAccount()
	checkPanic(err, fmt.Sprint(err))

	//send startup alert email with account info as data
	dataHTML := fmt.Sprintf(`<div style="background-color: ffffff;"><br/>&emsp;<strong>Account Summary:</strong><br/></div><div style="padding-left:25px;"><small><pre><code>%s</code></pre></small></div>`, acct.String())
	sendEmail(emailSubjects["app"], "greenAlertMessageData",
		//alert
		emailAlerts["tradingOpen"],

		//message
		strings.Replace(emailMessages["tradingOpen"], "{{tradeStartTimeStamp}}", tradeStartTimeStamp, 1),

		//data
		dataHTML,
	)
}

func sendAppFlyCompleteEmail() {
	//get current timestamp
	nowTimeStamp, _ := getTimeStampNowEST(appTimeFormat)

	//print account summary at startup
	acct, err := getAccount()
	checkPanic(err, fmt.Sprint(err))

	//send startup alert email with account info as data
	dataHTML := fmt.Sprintf(`<div style="background-color: ffffff;"><br/>&emsp;<strong>Account Summary:</strong><br/></div><div style="padding-left:25px;"><small><pre><code>%s</code></pre></small></div>`, acct.String())
	sendEmail(emailSubjects["app"], "greenAlertMessageData",
		//alert
		emailAlerts["tradingClose"],

		//message
		fmt.Sprintf(emailMessages["tradingClose"], nowTimeStamp),

		//data
		dataHTML,
	)
}

func sendAppUpCompleteEmail() {
	//get current timestamp
	nowTimeStamp, _ := getTimeStampNowEST(appTimeFormat)

	//print account summary at startup
	acct, err := getAccount()
	checkPanic(err, fmt.Sprint(err))

	//send startup alert email with account info as data
	dataHTML := fmt.Sprintf(`<div style="background-color: ffffff;"><br/>&emsp;<strong>Account Summary:</strong><br/></div><div style="padding-left:25px;"><small><pre><code>%s</code></pre></small></div>`, acct.String())
	sendEmail(emailSubjects["app"], "greenAlertMessageData",
		//alert
		emailAlerts["appStop"],

		//message
		fmt.Sprintf(emailMessages["appStop"], nowTimeStamp),

		//data
		dataHTML,
	)
}

func sendAppTradeReportEmail(reportJSON string) {
	//get current timestamp
	nowTimeStamp, _ := getTimeStampNowEST(appTimeFormat)

	//TODO - if overall progress (balance) is positive, use green alert; else, use red
	templateName := "greenAlertMessageData"

	dataHTML := fmt.Sprintf(`<div style="background-color: ffffff;"><br/>&emsp;<strong>Trade Report:</strong><br/></div><div style="padding-left:25px;"><small><pre><code>%s</code></pre></small></div>`, reportJSON)

	//send alert email with HTML trade report as data
	sendEmail(emailSubjects["tradeReport"], templateName,
		//alert
		emailAlerts["tradeReport"],

		//message
		fmt.Sprintf(emailMessages["tradeReport"], nowTimeStamp),

		//data
		dataHTML,
	)
}

func renderEmailTemplate(templateName string, data []string) string {
	var renderedHTML string

	switch templateName {
	case "greenAlertMessageData":
		renderedHTML = renderGreenAlertMessageData(data)
	case "redAlertMessageData":
		renderedHTML = renderRedAlertMessageData(data)
	default:
		renderedHTML = renderGreenAlertMessageData(data)
	}

	return renderedHTML
}

func renderGreenAlertMessageData(dataHTML []string) string {
	var alert, message, data string

	//handle varargs
	dataCount := len(dataHTML)
	switch {
	case dataCount >= 3:
		alert, message, data = dataHTML[0], dataHTML[1], dataHTML[2]
	case dataCount >= 2:
		alert, message, data = dataHTML[0], dataHTML[1], dataHTML[1]
	default:
		alert, message, data = dataHTML[0], dataHTML[0], dataHTML[0]
	}

	html := fmt.Sprintf(`<div style="background-color: #FF9A26;">
			<h1><span style="color: #00ff00; background-color: #005500;">ALERT: {{alert}}</span></h1>
			<h2 style="margin-top:25px; text-decoration: underline; color: #0000ff;">&emsp; %s v%s &emsp;</h2><hr/>
			</div>
			{{message}}
			<br/><br/>
			<div style="background-color: #FF9A26;"><hr/><h2 style="text-align: center; color: #005500;">-----------Data-----------</h2></strong><hr/><br/>
			</div>
			{{data}}`, appTitle, appVersion,
	)

	return strings.Replace(strings.Replace(strings.Replace(html, "{{alert}}", alert, 1), "{{data}}", data, 1), "{{message}}", message, 1)
}

func renderRedAlertMessageData(dataHTML []string) string {
	var alert, message, data string

	//handle varargs
	dataCount := len(dataHTML)
	switch {
	case dataCount >= 3:
		alert, message, data = dataHTML[0], dataHTML[1], dataHTML[2]
	case dataCount >= 2:
		alert, message, data = dataHTML[0], dataHTML[1], dataHTML[1]
	default:
		alert, message, data = dataHTML[0], dataHTML[0], dataHTML[0]
	}

	html := fmt.Sprintf(`<div style="background-color: #FF9A26;">
			<h1><span style="color: #ff0000; background-color: #750806;">ALERT: {{alert}}</span></h1>
			<h2 style="margin-top:25px; text-decoration: underline; color: #0000ff;">&emsp; %s v%s &emsp;</h2><hr/>
			</div>
			{{message}}
			<br/><br/>
			<div style="background-color: #FF9A26;"><hr/><h2 style="text-align: center; color: #600806;">-----------Data-----------</h2></strong><hr/><br/>
			</div>
			{{data}}`, appTitle, appVersion,
	)

	return strings.Replace(strings.Replace(strings.Replace(html, "{{alert}}", alert, 1), "{{data}}", data, 1), "{{message}}", message, 1)
}

func prepareEmailContent() {
	emailSubjects = map[string]string{
		"app":         fmt.Sprintf("%s v%s - %v - App Lifecycle", appTitle, appVersion, getTimeNowEST().Format(appTimeFormat)),
		"market":      fmt.Sprintf("%s v%s - %v - Market Activity", appTitle, appVersion, getTimeNowEST().Format(appTimeFormat)),
		"tradeReport": fmt.Sprintf("%s v%s - %v - Trade Report", appTitle, appVersion, getTimeNowEST().Format(appTimeFormat)),
		"account":     fmt.Sprintf("%s v%s - %v - Account Activity", appTitle, appVersion, getTimeNowEST().Format(appTimeFormat)),
	}
	emailAlerts = map[string]string{
		"appStart":     "lyTer Up!",
		"appError":     "App Error!",
		"appStop":      "App exited successfully!",
		"marketOpen":   "Market Open!",
		"marketClose":  "Market Closed!",
		"tradeReport":  "Trade Report!",
		"tradingOpen":  "Now Trading!",
		"tradingClose": "Trading Closed!",
	}
	emailMessages = map[string]string{
		"appStart": fmt.Sprintf(`<div style="color: #0000ff; font-family: Arial; font-size: 18px;"><br/><strong>App is Up!</strong><br/><br/>
			&emsp;<span style="text-decoration: underline;">Start Time</span>: <span style="color: #000000;"><strong>%s</strong></span><br/><br/>
			&emsp;<span style="text-decoration: underline;">Active Strategy Name</span>: <span style="color: #000000;"><strong>%s</strong></span><br/><br/>
			&emsp;<span style="text-decoration: underline;">Log File Name</span>: <span style="color: #000000;"><strong>%s</strong></span><br/><br/><br/>
			<strong>Account summary included below:</strong><br/></div>`, appStartTimeStamp, portfolio.ActiveStrategy.GetName(), logFileName),
		"tradingOpen": fmt.Sprintf(`<div style="color: #0000ff; font-family: Arial; font-size: 18px;"><br/><strong>App trading is now in-flight!</strong><br/><br/>
			&emsp;<span style="text-decoration: underline;">Trading Start Time</span>: <span style="color: #000000;"><strong>{{tradeStartTimeStamp}}</strong></span><br/><br/>
			&emsp;<span style="text-decoration: underline;">Active Strategy</span>: <span style="color: #000000;"><strong>%s</strong><br/><br/>&emsp;&emsp;<small>%s</small></span><br/><br/>
			<span style="font-size: 23px; color: #0000ff;"><strong>Currencies Trading:&emsp;</strong></span><span style="color: #008600;">%s</span><br/><br/>
			<strong>Account summary included below:</strong><br/><br/></div>`, portfolio.ActiveStrategy.GetName(), strings.Replace(portfolio.ActiveStrategy.String(), "\n", "<br/>&emsp;&emsp;", -1), strings.Join(currencies, ", ")),
		"tradingClose": `<div style="color: #0000ff; font-family: Arial; font-size: 18px;"><br/><strong>App trading closed!</strong><br/><br/>
			&emsp;<span style="text-decoration: underline;">Stop Time</span>: <span style="color: #000000;"><strong>%s</strong></span><br/><br/><br/>
			<strong>Account summary included below:</strong><br/><br/></div>`,
		"appStop": `<div style="color: #0000ff; font-family: Arial; font-size: 18px;"><br/><strong>App is down!</strong><br/><br/>
			&emsp;<span style="text-decoration: underline;">Stop Time</span>: <span style="color: #000000;"><strong>%s</strong></span><br/><br/><br/>
			<strong>Account summary included below:</strong><br/><br/></div>`,
		"tradeReport": `<div style="color: #0000ff; font-family: Arial; font-size: 18px;"><br/><strong>Report of in-flight app trade progress.</strong><br/><br/>
			&emsp;<span style="text-decoration: underline;">Report Time</span>: <span style="color: #000000;"><strong>%s</strong></span><br/><br/>
			</div>`,
	}
}
