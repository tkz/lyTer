package lyter

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/tkz/lyTer/portfolio"
	"gitlab.com/tkz/lyTer/v20"
)

func prepareContent() {
	//prepare Active Strategy and any other portfolio init
	portfolio.PrepareStrategy()

	//  log file name
	logFileName = fmt.Sprintf("%s_%s", portfolio.ActiveStrategy.GetName(), appStartTime.Format(loggerFileNameFormat))

	//prepare email templates for email alerts
	prepareEmailContent()
}

/*
 * Util Funcs
 * ==========
 */

//checkPanic checks if there's an err and logs/panics if so
func checkPanic(err error, msg string) {
	if err != nil {
		log.Panicln(msg, "\n>>", err)
	}
}

//time
func getTimeStampNowEST(format string) (string, time.Time) {
	now := getTimeNowEST()

	return now.Format(format), now
}

func getTimeStampNextTradeWindowOpen() string {
	return ""
}

func getTimeNowEST() time.Time {
	//output
	adjustedTime := time.Now()

	timeLoc, _ := time.LoadLocation("EST")

	//adjust for daylight savings if necessary
	isDaylightSavings, _ := strconv.ParseBool(v20.GetEnv("isDaylightSavings"))
	if isDaylightSavings {
		adjustedTime = adjustedTime.Add(time.Hour)
	}

	return adjustedTime.In(timeLoc)
}

func getTimeNowLoc(location string) time.Time {
	//output
	adjustedTime := time.Now()

	timeLoc, _ := time.LoadLocation(location)

	//adjust for daylight savings if necessary
	isDaylightSavings, _ := strconv.ParseBool(v20.GetEnv("isDaylightSavings"))
	if isDaylightSavings {
		adjustedTime = adjustedTime.Add(time.Hour)
	}

	return adjustedTime.In(timeLoc)
}

//returns true if trading window is open at given time; else, false
func isTradeWindowOpen(then time.Time) bool {
	//if it's a middle weekday, window is open 24hrs
	if then.Weekday() > marketOpenWeekday && then.Weekday() < marketCloseWeekday {
		return true
	}

	//if it's a weekend, window is closed
	if then.Weekday() > marketCloseWeekday || then.Weekday() < marketOpenWeekday {
		return false
	}

	//if day that window opens
	if then.Weekday() == marketOpenWeekday {
		//window open if tradeWindowOpenHour or later; else, closed
		if then.Hour() >= tradeWindowOpenHour {
			return true
		}

		return false
	}

	//if day that window closes
	if then.Weekday() == marketCloseWeekday {
		//window open if before tradeWindowCloseHour; else, closed
		if then.Hour() < tradeWindowCloseHour {
			return true
		}

		return false
	}

	//impossible?
	log.Panicf("impossible time encountered; 'then' time not picked up by logic in 'isTradeWindowOpen()'")
	return false
}

//logger
type logger struct {
	outFile    *os.File
	timeFormat string
}

func (writer *logger) Write(bytes []byte) (int, error) {

	return writer.outFile.Write([]byte(fmt.Sprintf("[%s] %s >> %s", appTitle, getTimeNowEST().Format(writer.timeFormat), string(bytes))))
}

//checkLogLevel tests current env log level against checkLevel to determine if log should occur
func checkLogLevel(checkLevel int) bool {
	//check env logLevel
	currentLevel, _ := strconv.Atoi(logLevel)

	//DEBUG
	//log.Printf("current log level: %d", currentLevel)

	return currentLevel >= checkLevel
}
