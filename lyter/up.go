package lyter

import (
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/tkz/lyTer/portfolio"
	"gitlab.com/tkz/lyTer/v20"
)

const (
	appTitle             = "lyTer"
	appVersion           = "0.0.1"
	loggerDirName        = "logs"
	loggerFileNameFormat = "2006_01_02_15_04_05"
	loggerTimeFormat     = "01/02 @ 3:04:05.999 PM"
	appTimeFormat        = "3:04:05.999 PM"
	tradeTimeFormat      = "01/02 @ 15:04:05.999"
	tradeWindowOpenHour  = 18
	marketOpenWeekday    = time.Weekday(0)
	tradeWindowCloseHour = 16
	marketCloseWeekday   = time.Weekday(5)
)

var (
	//app
	timeout             time.Duration
	logFileName         string
	appStartTime        time.Time
	appStartTimeStamp   string
	tradeStartTime      time.Time
	tradeStartTimeStamp string
	tradeStopTimeStamp  string
	appStopTimeStamp    string

	//FX
	currencies []string

	//Log Level
	logLevel = os.Getenv("LOG_LEVEL")
)

//Up brings the application to life
func Up() {
	//record startup time of app
	appStartTimeStamp, appStartTime = getTimeStampNowEST(appTimeFormat)

	//  timeout
	timeout, _ = time.ParseDuration(getEnv("timeout"))

	//  currencies
	currencies = strings.Split(getEnv("currencies"), " ")

	//prepare content such as portfolio, log file, and email templates; uses appStartTimeStampabove
	prepareContent()

	//DEBUG
	if checkLogLevel(2) {
		log.Printf("up: ActiveStrategy >>\n\n%s\n\n", portfolio.ActiveStrategy.String())
	} else {
		if checkLogLevel(1) {
			log.Printf("up: ActiveStrategy >> %s\n\n", portfolio.ActiveStrategy.GetName())
		}
	}

	//switch to custom file logger

	//  final log to console before switching to file
	log.Printf("up: app in progress..see file '%s' for further app logs\n\n", logFileName)

	//  create log dir if necessary
	logPath := filepath.Join(".", loggerDirName)
	if err := os.MkdirAll(logPath, os.ModePerm); err != nil {
		log.Panicf("Could not create directory '%s", logPath)
	}

	//  change to logPath dir
	if err := os.Chdir(logPath); err != nil {
		log.Panicf("Could not change to directory '%s", logPath)
	}

	f, err := os.OpenFile(logFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()

	log.SetFlags(0)
	log.SetOutput(&logger{
		outFile:    f,
		timeFormat: loggerTimeFormat,
	})

	//alert that app is up (log/email)
	alertAppUp()

	//if Active Strategy isCandleAligned, wait for alignment with trend candle
	durationTrendCandle := v20.CandleGranDurMap[portfolio.ActiveStrategy.GetTrendGran()]
	if now, timeAfterCandle := getTimeNowEST(), getTimeNowEST().Add(durationTrendCandle); isTradeWindowOpen(now) && isTradeWindowOpen(timeAfterCandle) {
		if portfolio.ActiveStrategy.GetIsCandleAligned() {
			//get time @ next candle start
			var timeNextCandleStart time.Time

			//set up request for current (trend) candle
			currentCandleReq := v20.CandlestickGetReqWrapper{
				Instrument:  currencies[0],
				Price:       "M",
				Granularity: portfolio.ActiveStrategy.GetTrendGran(),
				Count:       1,
				//Smooth:      true,
				//Print:       true,
			}
			currentCandleResponse, err := v20.GetCandlesticksResponse(currentCandleReq)
			if err != nil || len(currentCandleResponse.Candles) == 0 {
				log.Panicf("error: could not retrieve current candle for time alignment\n")
			}

			currentCandle := currentCandleResponse.Candles[0]

			timeNextCandleStart = v20.CandleToLocalTime(currentCandle.Time).Add(durationTrendCandle)

			//DEBUG
			//log.Printf("timeNextCandleStart: %v", timeNextCandleStart.Format(loggerTimeFormat))

			for {
				now = getTimeNowEST()
				if now.After(timeNextCandleStart) {
					timeNextCandleStart = timeNextCandleStart.Add(durationTrendCandle)
				} else {
					break
				}
			}

			//sleep until next trend candle begins
			waitAlignDuration := timeNextCandleStart.Sub(now) - portfolio.ActiveStrategy.GetHeartbeat()
			if waitAlignDuration < 0 {
				waitAlignDuration = timeNextCandleStart.Sub(now)
				log.Printf("up: Active Strategy isAlignCandlestick..resuming on %v", timeNextCandleStart.Add(portfolio.ActiveStrategy.GetHeartbeat()).Format(loggerTimeFormat))
			} else {
				log.Printf("up: Active Strategy isAlignCandlestick..resuming on %v", timeNextCandleStart.Format(loggerTimeFormat))
			}

			//log
			log.Printf("up: ..time until ticker starts: %v", waitAlignDuration)

			time.Sleep(waitAlignDuration)
		}
		//else, continue on to fly
	} else {
		//sleep until market next opens

		//set days duration
		var days int
		//if sunday..
		if now.Weekday() == time.Weekday(0) {
			days = 0
		} else {
			days = int(time.Weekday(7) - now.Weekday())
		}

		//time on day when market next opens
		timeOpeningDay := now.Add(time.Hour * 24 * time.Duration(days))

		//get time @ next trade window open
		timeNextTradeWindowOpen := time.Date(timeOpeningDay.Year(), timeOpeningDay.Month(), timeOpeningDay.Day(), tradeWindowOpenHour, 0, 0, 0, timeOpeningDay.Location())

		//log
		log.Printf("up: waiting for trading window to open\n..resuming on %v", timeNextTradeWindowOpen.Format(loggerTimeFormat))

		//wait until trading window opens
		time.Sleep(timeNextTradeWindowOpen.Sub(now))
	}

	//create new app ticker with strategy heartbeat duration
	ticker := time.NewTicker(portfolio.ActiveStrategy.GetHeartbeat())

	//record startup time of fly(), adjusted by one heartbeat
	tradeStartTime = getTimeNowEST().Add(portfolio.ActiveStrategy.GetHeartbeat())
	tradeStartTimeStamp = tradeStartTime.Format(appTimeFormat)

	//launch main go routine that runs portfolio.ActiveStrategy
	go fly(ticker)

	//alert that app is trading/in-flight (log/email)
	alertAppFly()

	//wait until timeout (if specified) or next market close, whichever comes first
	now := tradeStartTime

	//  time on day when market next closes
	timeClosingDay := now.Add(time.Hour * 24 * time.Duration(marketCloseWeekday-now.Weekday()))

	//  get time @ next trade window close
	timeNextTradeWindowClose := time.Date(timeClosingDay.Year(), timeClosingDay.Month(), timeClosingDay.Day(), tradeWindowCloseHour, 0, 0, 0, timeClosingDay.Location())

	//if the next market close is after timeout, only sleep until timeout
	if timeNextTradeWindowClose.After(now.Add(timeout)) {
		//snooze main thread until timeout
		log.Printf("up: timeout is set to %v.  Fly will exit on %v\n\n", timeout, now.Add(timeout).Format(loggerTimeFormat))

		time.Sleep(timeout)
	} else {
		//snooze main thread until trading window closes
		log.Printf("up: timeout is after trade window closing..\napp will exit @ trade window close on %v\n\n", timeNextTradeWindowClose.Format(loggerTimeFormat))

		time.Sleep(timeNextTradeWindowClose.Sub(now))
	}

	//cleanup (i.e. close any open trades)

	//stop ticker (stops go fly)
	ticker.Stop()

	//record stop time of fly()
	tradeStopTimeStamp, _ = getTimeStampNowEST(appTimeFormat)

	//alert that app is no longer trading/in-flight (log/email)
	alertAppFlyComplete()

	//record stop time of app (Up)
	appStopTimeStamp, _ = getTimeStampNowEST(appTimeFormat)

	//alert that app has wrapped up and will now exit (log/email)
	alertAppUpComplete()

	os.Exit(0)
}
