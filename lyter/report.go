package lyter

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/tkz/lyTer/v20"
)

type closedTradeSummary struct {
	RealizedPL string `json:"realizedPL"`
	Instrument string `json:"instrument"`
	Units      string `json:"units"`
	OpenTime   string `json:"openTime"`
	OpenPrice  string `json:"openPrice"`
	ClosePrice string `json:"closePrice"`
	CloseTime  string `json:"closeTime"`
}

type tradeReport struct {
	OpenTrades            []v20.Trade          `json:"openTrades"`
	TotalUnrealizedPL     string               `json:"totalUnrealizedPL"`
	TotalOpenTrades       int                  `json:"totalOpenTrades"`
	ClosedTrades          []closedTradeSummary `json:"closedTrades"`
	TotalRealizedPL       float64              `json:"totalRealizedPL"`
	TotalTradesClosed     int                  `json:"totalTradesClosed"`
	TotalPLPerClosedTrade float64              `json:"totalPLPerClosedTrade"`
	AccountBalance        string               `json:"accountBalance"`
}

func generateTradeReport() string {
	//output
	var reportJSON string

	//report struct to be marshalled
	report := tradeReport{}

	//get account
	account, err := v20.GetAccount()
	if err != nil {
		msg := fmt.Sprintf("error: could not retrieve account\n>>%v\n", err)
		log.Println(msg)
		return msg
	}

	//get open trades
	openTrades, err := v20.GetOpenTrades()
	if err != nil {
		msg := fmt.Sprintf("error: could not retrieve open trades\n>>%v\n", err)
		log.Println(msg)
		return msg
	}

	//get closed trade summaries via closed trades in transaction history
	closedTrades := []closedTradeSummary{}

	//  get all ORDER_FILL tx's since app started
	orderFillTransactions, err := v20.GetOrderFillTransactionsSince(tradeStartTime, []string{"ORDER_FILL"})
	if err != nil {
		msg := fmt.Sprintf("error: could not retrieve order fill transactions\n>>%v\n", err)
		log.Println(msg)
		return msg
	}

	//  search orderFillTransactions for closed trades
	for i, tx := range orderFillTransactions {
		if checkLogLevel(2) {
			log.Printf("orderFillTransaction #%d >>\n%s\n", i+1, tx.String())
		}

		//check if closed trade
		var summary closedTradeSummary
		if tx.TradesClosed != nil && len(tx.TradesClosed) > 0 {
			//get trade and append to closedTrades
			tradeSpec := tx.TradesClosed[0].TradeID
			closedTrade, err := v20.GetTrade(tradeSpec)
			if err != nil {
				msg := fmt.Sprintf("error: could not retrieve trade with tradeID '%s'\n>>%v\n", tradeSpec, err)
				log.Println(msg)
				return msg
			}

			//create summary
			summary = closedTradeSummary{
				RealizedPL: closedTrade.RealizedPL,
				Instrument: closedTrade.Instrument,
				Units:      closedTrade.InitialUnits,
				OpenTime:   closedTrade.OpenTime,
				OpenPrice:  closedTrade.Price,
				CloseTime:  closedTrade.CloseTime,
				ClosePrice: closedTrade.AverageClosePrice,
			}

			closedTrades = append([]closedTradeSummary{summary}, closedTrades...)
		}
	}

	//if no trades, return no activity
	if len(openTrades)+len(closedTrades) == 0 {
		msg := fmt.Sprintf("No trade activity since start of app trading @ %s", tradeStartTimeStamp)
		log.Println(msg)
		return msg
	}

	//build report

	//  OpenTrades
	report.OpenTrades = openTrades
	if checkLogLevel(2) {
		log.Printf("report.OpenTrades = %v", report.OpenTrades)
	}

	//  TotalUnrealizedPL
	report.TotalUnrealizedPL = account.UnrealizedPL
	if checkLogLevel(2) {
		log.Printf("report.TotalUnrealizedPL = %v", report.TotalUnrealizedPL)
	}

	//  TotalOpenTrades
	report.TotalOpenTrades = len(openTrades)
	if checkLogLevel(2) {
		log.Printf("report.TotalOpenTrades = %v", report.TotalOpenTrades)
	}

	//  ClosedTrades
	report.ClosedTrades = closedTrades
	if checkLogLevel(2) {
		log.Printf("report.ClosedTrades = %v", report.ClosedTrades)
	}

	//  TotalRealizedPL
	var realizedPL float64
	for _, trade := range closedTrades {
		realizedPL, _ = strconv.ParseFloat(trade.RealizedPL, 64)
		report.TotalRealizedPL += realizedPL
	}
	if checkLogLevel(2) {
		log.Printf("report.TotalRealizedPL = %v", report.TotalRealizedPL)
	}

	//  TotalTradesClosed
	report.TotalTradesClosed = len(closedTrades)
	if checkLogLevel(2) {
		log.Printf("report.TotalTradesClosed = %v", report.TotalTradesClosed)
	}

	//  TotalPerClosedTradePL
	if report.TotalTradesClosed > 0 {
		report.TotalPLPerClosedTrade = report.TotalRealizedPL / float64(report.TotalTradesClosed)
	}
	if checkLogLevel(2) {
		log.Printf("report.TotalPLPerClosedTrade = %v", report.TotalPLPerClosedTrade)
	}

	//  AccountBalance
	report.AccountBalance = account.Balance
	if checkLogLevel(2) {
		log.Printf("report.AccountBalance = %v", report.AccountBalance)
	}

	//END build report

	//marshal report structure to JSON
	jsonBytes, err := json.MarshalIndent(report, "", "  ")
	if err != nil {
		log.Printf("unable to marshal trade report to json!\n>>%v\n", err)
	}

	//convert to string
	reportJSON = string(jsonBytes)

	if checkLogLevel(2) {
		log.Printf("\nTrade Report\n============\n%s\n", reportJSON)
	}

	return reportJSON
}
