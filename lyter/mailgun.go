package lyter

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/url"
	"strings"
)

// ResponseSendEmail includes an id and message indicating if email sent successfully
type ResponseSendEmail struct {

	// The email identifier
	ID string `json:"id,omitempty"`

	Message string `json:"message,omitempty"`
}

//SendEmail sends an email to designated recipient
func sendEmail(subject string, templateName string, data ...string) error {
	//log action
	log.Printf("v20: Sending email notification to %s...", getEnv("mailgunRecipient"))

	//get email HTML
	emailHTML := renderEmailTemplate(templateName, data)

	//request
	err := requestSendEmail(subject, emailHTML)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("Email sent successfully!")
	}

	return err
}

//requestSendEmail returns a response from POST to Mailgun to send email
func requestSendEmail(subject string, html string) error {
	//define response type, req method, path w/params, and body
	response := &ResponseSendEmail{}
	method := "POST"
	path := getEnv("mailgunURL")

	//get from/to emails
	sender := getEnv("mailgunSender")
	recipient := getEnv("mailgunRecipient")

	//make form data
	formData := url.Values{}
	formData.Set("from", sender)
	formData.Set("to", recipient)
	formData.Set("subject", subject)
	formData.Set("html", html)

	//set custom headers
	headers := map[string]string{
		"Authorization": getEnv("mailgunAuthHeader"),
		"Content-Type":  "application/x-www-form-urlencoded",
		//"Content-Length": strconv.Itoa(len(formData.Encode())),
	}

	//execute request and get response
	rawResponse, err := sendRequest(method, path, strings.NewReader(formData.Encode()), headers, false)
	if err != nil {
		log.Printf("error executing new request >> method: %s, path: %s\n", method, path)
		return err
	}

	//ensure 200 status
	if rawResponse.Status != "200 OK" {
		return errors.New("received status code " + rawResponse.Status)
	}

	//read response
	defer rawResponse.Body.Close()
	responseBytes, err := ioutil.ReadAll(rawResponse.Body)
	if err != nil {
		log.Println("unable to read response of http request!")
		return err
	}

	//unmarshal JSON response to object
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		log.Println("unable to marshal valid response to json!")
		return err
	}

	return err
}
