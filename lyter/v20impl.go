package lyter

import (
	"io"
	"net/http"

	"gitlab.com/tkz/lyTer/v20"
)

/*
 * v20 Impl
 * ========
 */

func getEnv(propName string) string {
	return v20.GetEnv(propName)
}

func getAccount() (*v20.Account, error) {
	acct, err := v20.GetAccount()
	return acct, err
}

func sendRequest(method, path string, body io.Reader, headers map[string]string, isRelPath bool) (*http.Response, error) {
	return v20.SendRequest(method, path, body, headers, isRelPath)
}
