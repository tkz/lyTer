package lyter

import (
	"log"
	"time"

	"gitlab.com/tkz/lyTer/portfolio"
)

//fly is the main application go routine
func fly(ticker *time.Ticker) {
	//log
	log.Println("fly: Now Trading!")

	for range ticker.C {
		log.Printf("fly: run active strategy '%s'", portfolio.ActiveStrategy.GetName())

		//strategy result
		var strategyResult portfolio.StrategyResult

		//execute portfolio.ActiveStrategy and get StrategyResult
		strategyResult = portfolio.ActiveStrategy.Run(currencies)

		//print results if not OK
		if !strategyResult.OK() {
			log.Printf("fly: strategy results..\n\n%s\n\n", strategyResult.String())
		}

		//send trade report email at regular intervals
		if now := getTimeNowEST(); now.After(tradeStartTime.Add(time.Minute)) && now.Hour()%3 == 0 && now.Minute() == 0 && now.Second() < int(portfolio.ActiveStrategy.GetHeartbeat()/time.Second) {
			alertAppTradeReport()
		}
	}

	//log
	log.Println("fly: Trading Closed!")
}
