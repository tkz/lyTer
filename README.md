# lyTer

[![Build Status](https://gitlab.com/tkz/lyTer/badges/master/build.svg)](https://gitlab.com/tkz/lyTer/commits/master) [![Coverage Report](https://gitlab.com/tkz/lyTer/badges/master/coverage.svg)](https://gitlab.com/tkz/lyTer/commits/master)


## Execution

Using Docker:

Test
```
./tools/testLatestDocker.sh
```

Prod
```
./tools/prodLatestDocker.sh
```

## Author

* [TKZ](https://gitlab.com/tkz)

