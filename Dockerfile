# Base image: https://hub.docker.com/_/golang/
# This file is used for CI and testing purposes
FROM golang:latest

# Env
ENV GOBIN /go/bin
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
ENV GOOS linux
ENV GOARCH amd64
ENV CGO_ENABLED 1
ENV DEP_VERSION 0.4.1
ENV APP_PATH ${GOPATH}/src/gitlab.com/tkz/lyTer

RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH
RUN mkdir build

# Add apt key for LLVM repository
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Add LLVM apt repository
RUN echo "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-5.0 main" | tee -a /etc/apt/sources.list

# Install clang from LLVM repository
RUN apt-get update && apt-get install -y --no-install-recommends \
    clang-5.0 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN echo "export CC=clang-5.0" | tee -a ${set_clang} && chmod a+x ${set_clang}

# Install golint
RUN go get -u github.com/golang/lint/golint
RUN chmod +x $GOPATH/bin/golint

# Go dep!
# Download the binary to bin folder in $GOPATH
RUN curl -L -s https://github.com/golang/dep/releases/download/v${DEP_VERSION}/dep-linux-amd64 -o $GOPATH/bin/dep

# Make the binary executable
RUN chmod +x $GOPATH/bin/dep

COPY Gopkg.toml Gopkg.lock ./

RUN dep ensure -vendor-only

ADD . $APP_PATH

RUN go build .