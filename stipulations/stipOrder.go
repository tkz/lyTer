package stipulations

import (
	"gitlab.com/tkz/lyTer/v20"
)

var (
	//StipOrderConfig is config map of Stip
	StipOrderConfig map[string]Stip

	cacheOrder = map[string]map[string][]v20.Order{}
)

func prepareStipsOrder() {
	//declare/assign StipOrderConfig
	StipOrderConfig = map[string]Stip{
		"OpenLong": &stipOrderOpenLong{
			name: "OpenLong",
		},
	}

}
