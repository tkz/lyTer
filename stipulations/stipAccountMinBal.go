package stipulations

import (
	"fmt"
	"log"
	"strconv"

	"gitlab.com/tkz/lyTer/v20"
)

type stipAccountMinBal struct {
	//stip account
	name   string
	data   *v20.Account
	result TradeStipResult

	//MinBal - evaluate()
	_minBal float64 //required
}

//Decide returns OK result iff all _dataReqs evaluate OK
func (sa *stipAccountMinBal) Decide() StipResult {
	//fetch account data
	sa.fetchData()

	//evaluate account balance against _minBal
	return sa.evaluate()
}

//representation of stip as a string
func (sa *stipAccountMinBal) String() string {
	return fmt.Sprintf("\n\nstipAccountMinBal\n\t===============\n\tname: %s\n\tdata: %s\n\t_minBal: %v\n\tresult: %s", sa.name, sa.data.String(), sa._minBal, sa.result.String())
}

//evaluates this stip, returning StipResult
func (sa *stipAccountMinBal) evaluate() StipResult {
	//output
	result := TradeStipResult{Name: sa.name, IsOK: true}

	//check account balance against _minBal
	if bal, _ := strconv.ParseFloat(sa.data.Balance, 64); sa._minBal <= bal {
		//rating: balance is sufficient
		result.Rating += 10
	} else {
		log.Printf("account balance '%f' is less than minimum of '%f'", bal, sa._minBal)
		result.IsOK = false
	}

	return &result
}

//fetches latest data from API
func (sa *stipAccountMinBal) fetchData() {
	//get current account summary
	sa.data, _ = v20.GetAccount()
}
