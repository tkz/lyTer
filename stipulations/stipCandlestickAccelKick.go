package stipulations

import (
	"errors"
	"fmt"
	"log"
	"math"
	"strconv"

	"gitlab.com/tkz/lyTer/v20"
)

type stipCandlestickAccelKick struct {
	//stipCandlestick
	name      string
	_dataReqs []v20.CandlestickGetReqWrapper
	dataLast  map[string]map[string]v20.Candlestick
	dataNew   map[string]map[string][]v20.Candlestick
	dataCache map[string]map[string][]v20.Candlestick
	result    TradeStipResult

	//AccelKick - evaluate()
	_trendLength int //required
	currency     string
	granularity  string
}

//Decide returns OK result iff all _dataReqs evaluate OK
func (sc *stipCandlestickAccelKick) Decide() StipResult {
	//output
	result := TradeStipResult{Name: sc.name, IsOK: true}

	//fetch candlestick data
	sc.fetchData()

	//evaluate data from each request
	for _, req := range sc._dataReqs {
		sc.currency = req.Instrument
		sc.granularity = req.Granularity

		reqResult := sc.evaluate().(*TradeStipResult)
		reqResult.Msg = fmt.Sprintf("instrument: %s, granularity: %s", req.Instrument, req.Granularity)

		//if a non-OK reqResult is found, fail this stip
		if !reqResult.IsOK {
			result.IsOK = false
			result.Status = 8001
			result.Err = fmt.Errorf("stipCandlestickAccelKick: trend not OK with %s", reqResult.Msg)
			return &result
		}
		//reqResults = append(reqResults)
	}

	return &result
}

//representation of stip as a string
func (sc *stipCandlestickAccelKick) String() string {
	return fmt.Sprintf("\n\tstipCandlestick\n\t===============\n\t_dataReqs: %v\n\tdataNew: %v\n\tdataCache: %v\nresult: %v", sc._dataReqs, sc.dataNew, sc.dataCache, sc.result.String())
}

//evaluates this stip, returning StipResult
func (sc *stipCandlestickAccelKick) evaluate() StipResult {
	//input
	candles := sc.dataCache[sc.currency][sc.granularity]

	//output
	result := TradeStipResult{Name: sc.name, IsOK: true}

	//valid trend length?
	if sc._trendLength > 1 {
		//rating: there are enough candles for the trend
		result.Rating += 10
	} else {
		log.Printf("trend length below 2")
		result.IsOK = false
		result.Status = 9000
		result.Err = errors.New("trend length below 2")
		return &result
	}

	//enough candles for the trend?
	candlesCount := len(candles)
	if candlesCount >= sc._trendLength {
		//rating: there are enough candles for the trend
		result.Rating += 10
	} else {
		log.Printf("not enough candles")
		result.IsOK = false
		result.Status = 9001
		result.Err = errors.New("not enough candles")
		return &result
	}

	//assign slice of length _trendLength to trendCandles
	trendCandles := candles[candlesCount-sc._trendLength:]

	//lead candle body gain/loss (+/-)
	var leadCandleBody, candleBody float64

	//see if all candles are increasing
	for i, candle := range trendCandles {
		//special case, if first candle, get direction
		if i == 0 {
			openPrice, _ := strconv.ParseFloat(candle.Mid.O, 64)
			closePrice, _ := strconv.ParseFloat(candle.Mid.C, 64)
			if closePrice-openPrice == 0 {
				log.Printf("no body on candle %d", i)
				result.IsOK = false
				result.Status = 9002
				result.Err = fmt.Errorf("no body on candle %d", i)
				return &result
			}

			//rating: the first candle has a body
			result.Rating += 10
			leadCandleBody = closePrice - openPrice

			//set result status
			switch true {
			case leadCandleBody > 0:
				result.Status = 1
			case leadCandleBody < 0:
				result.Status = -1
			default:
				result.Status = 0
			}

			continue
		}

		//get candle data
		prevClosePrice, _ := strconv.ParseFloat(trendCandles[i-1].Mid.C, 64)

		newHighPrice, _ := strconv.ParseFloat(candle.Mid.H, 64)
		newOpenPrice, _ := strconv.ParseFloat(candle.Mid.O, 64)
		newClosePrice, _ := strconv.ParseFloat(candle.Mid.C, 64)
		newLowPrice, _ := strconv.ParseFloat(candle.Mid.L, 64)

		candleBody = newClosePrice - prevClosePrice

		//look for elimination criteria, else return ok
		if candleBody == 0 {
			log.Printf("no body on candle %d", i)
			result.IsOK = false
			result.Status = 9002
			result.Err = fmt.Errorf("no body on candle %d", i)
			return &result
		}

		if candleBody > 0 && leadCandleBody < 0 || candleBody < 0 && leadCandleBody > 0 {
			result.IsOK = false
			result.Status = 9003

			var found, wanted string
			if candleBody > 0 {
				found = "long"
				wanted = "short"
			} else {
				found = "short"
				wanted = "long"
			}
			result.Err = fmt.Errorf("%s candle found where expecting %s candle: %d", found, wanted, i)

			return &result
		}

		//check newest candle is larger than prev w/on-trend wicks
		if i == len(trendCandles)-1 {
			prevOpenPrice, _ := strconv.ParseFloat(trendCandles[i-1].Mid.O, 64)
			prevCandleBody := prevClosePrice - prevOpenPrice

			if math.Abs(candleBody) <= math.Abs(prevCandleBody) {
				log.Printf("latest candle not showing acceleration >> prev: %f, latest: %f", prevCandleBody, candleBody)
				result.IsOK = false
				result.Status = 9004
				result.Err = fmt.Errorf("most recent candle not showing acceleration >> prev: %f, latest: %f", prevCandleBody, candleBody)

				return &result
			}

			//rating: made it to last candle, which has a body, moves in same direction as first candle
			result.Rating += 10

			//check for on-trend wicks
			var topWick, bottomWick float64

			if candleBody > 0 {
				topWick = newHighPrice - newOpenPrice
				bottomWick = newClosePrice - newLowPrice
				if topWick > bottomWick {
					log.Printf("latest candle not showing on-trend wicks >> top: %f, bottom: %f", topWick, bottomWick)
					result.IsOK = false
					result.Status = 9005
					result.Err = fmt.Errorf("latest candle not showing on-trend wicks >> top: %f, bottom: %f", topWick, bottomWick)

					return &result
				}
			} else {
				topWick = newHighPrice - newOpenPrice
				bottomWick = newClosePrice - newLowPrice
				if bottomWick > topWick {
					log.Printf("latest candle not showing on-trend wicks >> top: %f, bottom: %f", topWick, bottomWick)
					result.IsOK = false
					result.Status = 9005
					result.Err = fmt.Errorf("latest candle not showing on-trend wicks >> top: %f, bottom: %f", topWick, bottomWick)

					return &result
				}
			}

			//rating: last candle's wicks are on-trend
			result.Rating += 10
			log.Printf("trend OK!")
		}
	}

	return &result
}

//fetches latest data from API and updates cacheCandlestick
func (sc *stipCandlestickAccelKick) fetchData() {
	//update all request wrappers based on cacheCandlestick
	for i, req := range sc._dataReqs {
		//update requests based on cache, if present
		if len(cacheCandlestick[req.Instrument][req.Granularity]) > 0 {
			sc._dataReqs[i] = getCacheUpdatedCandlesRW(req)
		}
	}

	//process requests concurrently and get responses
	responses, _ := v20.GoGetCandlesticksResponses(sc._dataReqs)

	//add response Candles to data map
	for _, resp := range responses {
		//get data to populate this stipCandlestick's dataNew and dataCache
		mergedCandles, newCandles := v20.GetMergedCompleteCandles(cacheCandlestick[resp.Instrument][resp.Granularity], resp.Candles)

		//assign dataCache as picture of full (updated) cache considered by this stip
		if _, ok := sc.dataCache[resp.Instrument]; ok {
			sc.dataCache[resp.Instrument][resp.Granularity] = mergedCandles
		} else {
			sc.dataCache[resp.Instrument] = map[string][]v20.Candlestick{
				resp.Granularity: mergedCandles,
			}
		}

		//assign dataNew as any new (complete) candles retrieved by this stip
		if _, ok := sc.dataNew[resp.Instrument]; ok {
			sc.dataNew[resp.Instrument][resp.Granularity] = newCandles
		} else {
			sc.dataNew[resp.Instrument] = map[string][]v20.Candlestick{
				resp.Granularity: newCandles,
			}
		}

		//assign dataLast as last candlestick for currency/granularity
		if _, ok := sc.dataLast[resp.Instrument]; ok {
			sc.dataLast[resp.Instrument][resp.Granularity] = resp.Candles[len(resp.Candles)-1]
		} else {
			sc.dataLast[resp.Instrument] = map[string]v20.Candlestick{
				resp.Granularity: resp.Candles[len(resp.Candles)-1],
			}
		}

		//update cacheCandlestick
		if _, ok := cacheCandlestick[resp.Instrument]; ok {
			cacheCandlestick[resp.Instrument][resp.Granularity] = mergedCandles
		} else {
			cacheCandlestick[resp.Instrument] = map[string][]v20.Candlestick{
				resp.Granularity: mergedCandles,
			}
		}

		//DEBUG
		if checkLogLevel(2) {
			log.Printf("Response candles:\n\tInstrument: %s\n\tGranularity: %s\n\tCount: %d\n", resp.Instrument, resp.Granularity, len(sc.dataNew[resp.Instrument][resp.Granularity]))
		}
	}
}
