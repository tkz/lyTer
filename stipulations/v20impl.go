package stipulations

import (
	"io"
	"log"
	"net/http"

	"gitlab.com/tkz/lyTer/v20"
)

var (
	currencies = ""
)

/*
 * v20 Impl
 * ========
 */

func getEnv(propName string) string {
	return v20.GetEnv(propName)
}

func getAccount() (*v20.Account, error) {
	acct, err := v20.GetAccount()
	return acct, err
}

func sendRequest(method, path string, body io.Reader, headers map[string]string, isRelPath bool) (*http.Response, error) {
	return v20.SendRequest(method, path, body, headers, isRelPath)
}

func getOpenPositions() []v20.Position {
	positions, _ := v20.GetOpenPositions()
	return positions
}

func getCacheUpdatedCandlesRW(req v20.CandlestickGetReqWrapper) v20.CandlestickGetReqWrapper {
	//if currency[granularity] in cache, update params to only get latest
	if cache, ok := cacheCandlestick[req.Instrument][req.Granularity]; ok {
		if len(cache) > 0 {
			//log
			logLevel := 2
			if checkLogLevel(logLevel) {
				log.Printf("getCacheUpdatedCandlesRW: trimmed request >>\n\told From: %v\n", req.From)
			}

			//Assign timestamp of last cached candle as 'From' and don't 'IncludeFirst'
			req.From = cache[len(cache)-1].Time
			req.IncludeFirst = false

			//log
			if checkLogLevel(logLevel) {
				log.Printf("\tnew From: %v\n\n", req.From)
			}
		}
	}

	return req
}
