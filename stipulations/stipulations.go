package stipulations

import (
	"fmt"
)

//Stip is the stipulation interface
type Stip interface {
	Decide() StipResult //returns StipResult to rule on whole stip
	String() string
	evaluate() StipResult //returns StipResult to rule on main stip logic
	fetchData()
}

//StipResult is a basic result interface
type StipResult interface {
	OK() bool
	String() string
}

//TradeStipResult is a general result struct used within package
type TradeStipResult struct {
	Name   string
	IsOK   bool
	Rating int
	Status int
	Msg    string
	Err    error
}

//OK returns IsOK
func (tsr *TradeStipResult) OK() bool {
	return tsr.IsOK
}

//String returns string representation of tsr
func (tsr *TradeStipResult) String() string {
	return fmt.Sprintf("\nTradeStipResult\n===============\nname: %s\nisOK: %v\nrating: %d\nstatus: %d\nmsg: %s\nerr: %v", tsr.Name, tsr.IsOK, tsr.Rating, tsr.Status, tsr.Msg, tsr.Err)
}
