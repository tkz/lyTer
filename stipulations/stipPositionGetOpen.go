package stipulations

import (
	"fmt"
	"log"

	"gitlab.com/tkz/lyTer/v20"
)

type stipPositionGetOpen struct {
	//stip position
	name   string
	data   []v20.Position
	result TradeStipResult
}

//returns result from evaluate
func (sp *stipPositionGetOpen) Decide() StipResult {
	//fetch open positions
	sp.fetchData()

	//evaluate account balance against _minBal
	return sp.evaluate()
}

//representation of stip as a string
func (sp *stipPositionGetOpen) String() string {
	return fmt.Sprintf("\n\nstipPositionGetOpen\n\t===============\n\tname: %s\n\tpositions: %d\n\tresult: %s", sp.name, len(sp.data), sp.result.String())
}

//evaluates this stip, returning StipResult
func (sp *stipPositionGetOpen) evaluate() StipResult {
	//output
	result := TradeStipResult{Name: sp.name, IsOK: true}

	//get number of open positions as rating
	result.Rating = len(sp.data)

	return &result
}

//fetches latest data from API
func (sp *stipPositionGetOpen) fetchData() {
	//get current account summary
	data, err := v20.GetOpenPositions()
	sp.data = data
	if err != nil && checkLogLevel(0) {
		log.Printf("error getting open positions: %v", err)
	}
}
