package stipulations

import (
	"gitlab.com/tkz/lyTer/v20"
)

var (
	//StipAccountConfig is config map of Stip
	StipAccountConfig map[string]Stip

	cacheAccount = map[string]map[string][]v20.Account{}
)

func prepareStipsAccount() {
	//declare/assign StipAccountConfig
	StipAccountConfig = map[string]Stip{}
}
