package stipulations

import (
	"os"
)

var (
	//ENV
	env = map[string]string{
		"logLevel": os.Getenv("LOG_LEVEL"),
	}
)
