package stipulations

import (
	"gitlab.com/tkz/lyTer/v20"
)

var (
	//StipPositionConfig is config map of Stip
	StipPositionConfig map[string]Stip

	cachePosition = map[string]map[string][]v20.Position{}
)

func prepareStipsPosition() {
	//declare/assign StipPositionConfig
	StipPositionConfig = map[string]Stip{
		"GetOpen": &stipPositionGetOpen{
			name: "GetOpen",
		},
	}

}
