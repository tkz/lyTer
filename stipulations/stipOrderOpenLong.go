package stipulations

import (
	"fmt"

	"gitlab.com/tkz/lyTer/v20"
)

type stipOrderOpenLong struct {
	//stip order
	name   string
	data   []v20.Order
	result TradeStipResult
}

//returns result from evaluate
func (so *stipOrderOpenLong) Decide() StipResult {
	//fetch open positions
	so.fetchData()

	//evaluate account balance against _minBal
	return so.evaluate()
}

//representation of stip as a string
func (so *stipOrderOpenLong) String() string {
	return fmt.Sprintf("\n\nstipOrderOpenLong\n\t===============\n\tname: %s\n\tpositions: %d\n\tresult: %s", so.name, len(so.data), so.result.String())
}

//evaluates this stip, returning StipResult
func (so *stipOrderOpenLong) evaluate() StipResult {
	//output
	result := TradeStipResult{Name: so.name, IsOK: true}

	//get number of open positions as rating
	result.Rating = len(so.data)

	return &result
}

//fetches latest data from API
func (so *stipOrderOpenLong) fetchData() {
	//TODO
}
