package stipulations

import (
	"gitlab.com/tkz/lyTer/v20"
)

var (
	//StipCandlestickConfig is config map of Stip
	StipCandlestickConfig map[string]Stip

	cacheCandlestick = map[string]map[string][]v20.Candlestick{}
)

func prepareStipsCandlestick() {
	//declare/assign StipCandlestickConfig
	StipCandlestickConfig = map[string]Stip{
		"AccelKick": &stipCandlestickAccelKick{
			name:         "AccelKick",
			_trendLength: 3,
			_dataReqs: []v20.CandlestickGetReqWrapper{
				v20.CandlestickGetReqWrapper{
					Instrument:  getEnv("currencies"),
					Price:       "M",
					Granularity: "M5",
					Print:       true,
				},
			},
		},
	}
}
