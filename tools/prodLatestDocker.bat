REM Login to gitlab docker registry
docker login registry.gitlab.com

REM Pull latest version of release image
docker pull registry.gitlab.com/tkz/lyter:latest

REM Runs container w/restart until stopped
REM Utilizes external .env file
docker run --name lyter_prod --restart=unless-stopped -d --env-file ./.env-prod registry.gitlab.com/tkz/lyter:latest
