#!/bin/bash
#
# Code coverage

#COVERAGE_DIR="${COVERAGE_DIR:-coverage}"
COVERAGE_DIR="coverage"

# Create the coverage files directory
mkdir -p "$COVERAGE_DIR" || true;
cd "$COVERAGE_DIR"

# Create html report for httpstatspan
echo ${package1##*/} ;
go test -coverprofile "${LOCAL_PKG##*/}.cov" "$LOCAL_PKG" && \
go tool cover -func="${LOCAL_PKG##*/}.cov" && \
go tool cover -html="${LOCAL_PKG##*/}.cov" -o "${LOCAL_PKG##*/}-coverage.html" || true ;
