#!/bin/bash
#
# Login and pull latest release image

sudo docker login registry.gitlab.com
sudo docker pull registry.gitlab.com/tkz/lyter:latest

# Requires filling in env vars
sudo docker run --name lyter_test \
    --restart=unless-stopped -d \
    --env-file ./.env \
    registry.gitlab.com/tkz/lyter:latest
