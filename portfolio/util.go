package portfolio

import (
	"log"
	"strconv"
	"time"

	"gitlab.com/tkz/lyTer/v20"
)

/*
 * Util Funcs
 * ==========
 */

//checkPanic checks if there's an err and logs/panics if so
func checkPanic(err error, msg string) {
	if err != nil {
		log.Panicln(msg, "\n>>", err)
	}
}

//time
func getTime(format string) string {
	return getTimeNowEST().Format(format)
}

func getTimeNowEST() time.Time {
	//output
	adjustedTime := time.Now()

	timeLoc, _ := time.LoadLocation("EST")

	//adjust for daylight savings if necessary
	isDaylightSavings, _ := strconv.ParseBool(v20.GetEnv("isDaylightSavings"))
	if isDaylightSavings {
		adjustedTime = adjustedTime.Add(time.Hour)
	}

	return adjustedTime.In(timeLoc)
}

func getTimeNowLoc(location string) time.Time {
	//output
	adjustedTime := time.Now()

	timeLoc, _ := time.LoadLocation(location)

	//adjust for daylight savings if necessary
	isDaylightSavings, _ := strconv.ParseBool(v20.GetEnv("isDaylightSavings"))
	if isDaylightSavings {
		adjustedTime = adjustedTime.Add(time.Hour)
	}

	return adjustedTime.In(timeLoc)
}

//checkLogLevel tests current env log level against checkLevel to determine if log should occur
func checkLogLevel(checkLevel int) bool {
	//check env logLevel
	currentLevel, _ := strconv.Atoi(env["logLevel"])

	return currentLevel >= checkLevel
}
