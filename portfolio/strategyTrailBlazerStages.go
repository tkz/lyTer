package portfolio

/*
import (
	"fmt"

	stips "gitlab.com/tkz/lyTer/stipulations"
)

//gate: number of open positions via result 'rating' field
type trailBlazerStage00 struct {
	name    string
	actions []*actionTrade
	result  stageResult
}

//executes the stage
func (tbs00 *trailBlazerStage00) perform() Result {
	//output
	result := stageResult{name: "IsOpenPositions", doNext: true, doIndex: 1}

	//should be only 1 action
	sResult := tbs00.actions[0].decide().(*stips.TradeStipResult)

	//transfer props to this result
	result.rating = sResult.Rating
	result.isOK = sResult.IsOK

	//set next steps
	if result.OK() {
		if result.rating > 0 {
			result.doIndex = 4
		}
	} else {
		result.doNext = false
	}

	//execute
	return &result
}

//String prints this stage
func (tbs00 *trailBlazerStage00) String() string {
	return "stage"
}

//no positions open so check trends, if a trend isOK then doNext to open trade
type trailBlazerStage01 struct {
	name    string
	actions []*actionTrade
	result  stageResult
}

//perform executes the stage
func (tbs01 *trailBlazerStage01) perform() Result {
	//output
	result := stageResult{name: "CheckOpen"}

	//set isOK to true if any pattern isOK
	for i := 0; i < len(tbs01.actions); i++ {
		pattResult := tbs01.actions[i].decide().(*stips.TradeStipResult)
		if pattResult.IsOK {
			result.isOK = true
			result.status = pattResult.Status
			result.msg = tbs01.actions[i].name //set name of OK pattern
			break
		}
	}

	//if result OK, check min balance
	if result.OK() {
		//set next steps
		result.doNext = true

		if result.status > 0 {
			//Open LONG
			result.doIndex = 2
		} else {
			//Open SHORT
			result.doIndex = 3
		}
	}

	return &result
}

//String prints this stage
func (tbs01 *trailBlazerStage01) String() string {
	return "stage"
}

//open LONG trade
type trailBlazerStage02 struct {
	name    string
	actions []*actionTrade
	result  stageResult
}

//perform executes the stage
func (tbs02 *trailBlazerStage02) perform() Result {
	//output
	result := stageResult{name: "OpenLong"}

	//execute trade action
	axResult := tbs02.actions[0].perform().(*actionTradeResult)

	result.isOK = axResult.isOK

	return &result
}

//String prints this stage
func (tbs02 *trailBlazerStage02) String() string {
	return "stage"
}

//open SHORT trade
type trailBlazerStage03 struct {
	name    string
	actions []*actionTrade
	result  stageResult
}

//perform executes the stage
func (tbs03 *trailBlazerStage03) perform() Result {
	//output
	result := stageResult{name: "OpenShort"}

	//execute open trade action
	axResult := tbs03.actions[0].perform().(*actionTradeResult)

	result.isOK = axResult.isOK

	return &result
}

//String prints this stage
func (tbs03 *trailBlazerStage03) String() string {
	return "stage"
}

//check for closing long trade
type trailBlazerStage04 struct {
	name    string
	actions []*actionTrade
	result  stageResult
}

//perform executes the stage
func (tbs04 *trailBlazerStage04) perform() Result {
	//output
	result := stageResult{name: "CheckClose", doIndex: 5}

	//set isOK to true if any pattern isOK
	for i := 0; i < len(tbs04.actions); i++ {
		pattResult := tbs04.actions[i].decide().(*stips.TradeStipResult)
		if pattResult.IsOK {
			result.isOK = true
			result.doNext = true
			result.status = pattResult.Status
			result.msg = fmt.Sprintf("%s isOK!", tbs04.actions[i].name)
			break
		}
	}

	return &result
}

//String prints this stage
func (tbs04 *trailBlazerStage04) String() string {
	return "stage"
}

//closes open position
type trailBlazerStage05 struct {
	name    string
	actions []*actionTrade
	result  stageResult
}

//perform executes the stage
func (tbs05 *trailBlazerStage05) perform() Result {
	//output
	result := stageResult{name: "CloseTrade"}

	//execute close trade action
	axResult := tbs05.actions[0].perform().(*actionTradeResult)

	result.isOK = axResult.isOK

	return &result
}

//String prints this stage
func (tbs05 *trailBlazerStage05) String() string {
	return "stage"
}
*/
