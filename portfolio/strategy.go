package portfolio

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/tkz/lyTer/v20"
)

var (
	//ActiveStrategy is the current strategy
	ActiveStrategy Strategy

	//Cache is the package level cache available for ActiveStrategy
	//Cache = map[string]map[string]interface{}{}
)

//PrepareStrategy runs assignment/init on components of selected strategy
func PrepareStrategy() {
	//if present/valid strategy config, make active
	strategyStruct := v20.GetEnv("activeStrategyStruct")

	var strategyParsed Strategy
	var strategyErr error

	//try to parse strategy from config JSON
	switch strategyStruct {
	case "strategyMagicCarpet":
		strategyParsed = &strategyMagicCarpet{}
	case "strategyTrailBlazer":
		strategyParsed = &strategyTrailBlazer{}
	default:
		log.Panicf("invalid or unsupported ACTIVE_STRATEGY_STRUCT detected from env")
	}

	//Attempt to parse strategy from env config
	strategyErr = json.Unmarshal([]byte(v20.GetEnv("activeStrategyConfig")), &strategyParsed)

	//validate strategy
	if strategyErr == nil {
		strategyErr = strategyParsed.Validate()
	}

	//if strategy is valid, make active
	if strategyErr == nil {
		ActiveStrategy = strategyParsed
	} else {
		//strategyErr, use default strategy
		log.Printf("portfolio: error parsing ACTIVE_STRATEGY_CONFIG:\n%v\n", strategyErr)

		//try to use default strategy
		strategyDefault := &strategyTrailBlazer{
			Name:            "TrailBlazer_v1.0_DEFAULT",
			TileSize:        2,
			SignalTiles:     3,
			RiskTiles:       3,
			RiskTilesOpen:   2,
			MaxTrades:       2,
			MarginUtil:      0.55,
			Heartbeat:       "30s",
			TrendGran:       "M5",
			IsCandleAligned: true,
		}

		//validate default strategy
		if err := strategyDefault.Validate(); err != nil {
			log.Panicf("portfolio: error parsing default strategy:\n%v\n", err)
		}

		log.Printf("portfolio: using default strategy '%s'.\n\n", strategyDefault.GetName())

		ActiveStrategy = strategyDefault
	}
}

//Strategy is what will be executed by lyter each tick
type Strategy interface {
	Run([]string) StrategyResult
	GetHeartbeat() time.Duration
	GetIsCandleAligned() bool
	GetName() string
	GetTrendGran() string
	String() string
	Validate() error
}

//StrategyResult contains data to prepare a report of Strategy status (recent/present)
type StrategyResult struct {
	name   string
	isOK   bool
	rating int
	status int
	msg    string
	err    error
}

//OK returns overall boolean whether strategy was executed successfully or not
func (sr *StrategyResult) OK() bool {
	return sr.isOK
}

//String returns string representation of this result
func (sr *StrategyResult) String() string {
	return fmt.Sprintf("strategyResult\n==============\nname: %s\nisOK: %v\nrating: %d\nstatus: %d\nmsg: %s\nerr: %v\n", sr.name, sr.isOK, sr.rating, sr.status, sr.msg, sr.err)
}
