package portfolio

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"time"

	"gitlab.com/tkz/lyTer/v20"
)

type strategyTrailBlazer struct {
	Name            string         `json:"name,omitempty"`
	Heartbeat       string         `json:"heartbeat,omitempty"`
	TrendGran       string         `json:"trendGran,omitempty"`
	TileSize        int            `json:"tileSize,omitempty"`
	SignalTiles     int            `json:"signalTiles,omitempty"`
	RiskTiles       int            `json:"riskTiles,omitempty"`
	RiskTilesOpen   int            `json:"riskTilesOpen,omitempty"`
	MaxTrades       int            `json:"maxTrades,omitempty"`
	MarginUtil      float64        `json:"marginUtil,omitempty"`
	IsCandleAligned bool           `json:"isCandleAligned,omitempty"`
	Result          StrategyResult `json:"-"`
}

func (stb *strategyTrailBlazer) Run(currencies []string) StrategyResult {
	//assign initial result
	result := StrategyResult{name: stb.Name, isOK: true}

	//=====
	//BEGIN - TrailBlazer Strategy Flow
	//=====

	if checkLogLevel(3) {
		log.Println("inside TrailBlazer Run!")
	}

	if len(currencies) < 1 {
		result.isOK = false
		result.msg = "error: no currencies provided to Run([]string) call"
		return result
	}

	//Get open trades
	openTrades, err := v20.GetOpenTrades()
	if err != nil {
		result.isOK = false
		return result
	}
	openTradeCount := len(openTrades)

	//tradeUpkeep if openTrades
	if checkLogLevel(1) {
		log.Printf("openTradeCount: %d", openTradeCount)
	}
	if openTradeCount > 0 {
		//update each trade
		for i, trade := range openTrades {
			if checkLogLevel(2) {
				log.Printf("#%d:\n\n%s", i+1, trade.String())
			}

			tradeCandles := stb.getActiveTradeCandles(trade)

			//adjust S/L and T/P orders if necessary
			result.isOK = stb.tradeUpkeep(trade, tradeCandles)
		}
	}

	//filter availableCurrencies by openTrades
	var availableCurrencies []string
	if openTradeCount > 0 {
		if openTradeCount < stb.MaxTrades {
			availableCurrencies = []string{}
			for _, currency := range currencies {
				var foundCurrency bool
				for _, trade := range openTrades {
					if trade.Instrument == currency {
						foundCurrency = true
						break
					}
				}
				if !foundCurrency {
					availableCurrencies = append(availableCurrencies, currency)
				}
			}
		} else {
			if checkLogLevel(1) {
				log.Printf("open trade maximum reached: %d\n", stb.MaxTrades)
			}
			return result
		}
	} else {
		availableCurrencies = currencies
	}

	if checkLogLevel(2) {
		log.Printf("availableCurrencies: %v", availableCurrencies)
	}

	if len(availableCurrencies) < 1 {
		result.msg = "skip open new positions: no availableCurrencies"
		return result
	}

	//set up request for current (trend) candle start time
	currentCandleReq := v20.CandlestickGetReqWrapper{
		Instrument:  availableCurrencies[0],
		Price:       "M",
		Granularity: stb.TrendGran,
		Count:       1,
		//Smooth:      true,
		//Print:       true,
	}
	currentCandleResponse, err := v20.GetCandlesticksResponse(currentCandleReq)
	if err != nil || len(currentCandleResponse.Candles) == 0 {
		log.Printf("error: could not retrieve current candle\n")
		result.isOK = false
		return result
	}

	currentCandle := currentCandleResponse.Candles[0]

	timeStartCurrentCandle := currentCandle.Time

	//skip opening new positions if 3 ticks have passed in current trend candle
	localTimeStartCurrentCandle := v20.CandleToLocalTime(timeStartCurrentCandle)

	if checkLogLevel(3) {
		log.Printf("currentCandle.Time: %v; localTimeStartCurrentCandle: %v", timeStartCurrentCandle, localTimeStartCurrentCandle)
	}

	//if time for candle has lapsed, go with next candle start time
	if now := getTimeNowEST(); now.Add(time.Second).After(localTimeStartCurrentCandle.Add(v20.CandleGranDurMap[stb.TrendGran])) {
		localTimeStartCurrentCandle = localTimeStartCurrentCandle.Add(v20.CandleGranDurMap[stb.TrendGran])
	}

	if getTimeNowEST().After(localTimeStartCurrentCandle.Add(stb.GetHeartbeat() * 3)) {
		log.Println("skip open new positions: trend candle in progress at least 3 heartbeats")
		return result
	}

	//conditional execution of new trade
	for i := 0; i < len(availableCurrencies) && openTradeCount < stb.MaxTrades; i++ {
		//STRATEGY: ensure no ORDER_FILL transactions within current candle for current currency

		if checkLogLevel(1) {
			log.Printf("evaluating instrument '%s'..", availableCurrencies[i])
		}

		//decide on execution
		if decision := stb.decide(stb.getActiveTrendCandles(localTimeStartCurrentCandle.Add(time.Second), availableCurrencies[i])); decision != 0 {
			//no execution if ORDER_FILL transactions found within last trend candle
			candleTransactions, err := v20.GetTransactionsSince(localTimeStartCurrentCandle, []string{"ORDER_FILL"})
			if err != nil {
				log.Printf("error: could not retrieve all requested transactions\n>>%v", err)
				result.isOK = false
				return result
			}

			//only consider transactions matching this currency
			matchingTransactions := []v20.Transaction{}
			for _, tx := range candleTransactions {
				if tx.Instrument == availableCurrencies[i] {
					matchingTransactions = append(matchingTransactions, tx)
				}
			}

			if checkLogLevel(2) {
				log.Printf("transactions found: %d", len(matchingTransactions))
			}

			//if there are ORDER_FILL transactions within current trend candle, return
			if len(matchingTransactions) > 0 {
				//don't open new position yet
				if checkLogLevel(1) {
					log.Printf("%s: trade not opened >> existing ORDER_FILL transaction(s) found within current candlestick.", availableCurrencies[i])
				}
				continue
			}

			//execute actions if necessary
			result.isOK = stb.execute(decision, availableCurrencies[i], openTrades)
			if result.isOK {
				openTradeCount++
			} else {
				result.msg = "could not execute trade successfully"
			}
		}
	}

	//assign/return result
	stb.Result = result
	return result
}

func (stb *strategyTrailBlazer) GetIsCandleAligned() bool {
	return stb.IsCandleAligned
}

func (stb *strategyTrailBlazer) GetHeartbeat() time.Duration {
	heartbeatDuration, err := time.ParseDuration(stb.Heartbeat)
	if err != nil {
		log.Panicf("stb heartbeat '%s' is not a valid duration string", stb.Heartbeat)
	}

	return heartbeatDuration
}

func (stb *strategyTrailBlazer) GetName() string {
	return stb.Name
}

func (stb *strategyTrailBlazer) GetTrendGran() string {
	return stb.TrendGran
}

func (stb *strategyTrailBlazer) String() string {
	return fmt.Sprintf("Strategy TrailBlazer\n====================\nName: %s\nTileSize: %d\nSignalTiles: %d\nRiskTiles: %d\nRiskTilesOpen: %d\nMaxTrades: %d\nMarginUtil: %f\nHeartbeat: %s\nTrendGran: %s\nIsCandleAligned: %v\n", stb.Name, stb.TileSize, stb.SignalTiles, stb.RiskTiles, stb.RiskTilesOpen, stb.MaxTrades, stb.MarginUtil, stb.Heartbeat, stb.TrendGran, stb.IsCandleAligned)
}

func (stb *strategyTrailBlazer) Validate() error {
	//output
	var validationErr error

	//error message
	errMsg := ""

	if stb.Name == "" {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'Name'\n"
	}

	if stb.TileSize < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'TileSize'\n"
	}

	if stb.SignalTiles < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'SignalTiles'\n"
	}

	if stb.RiskTiles < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'RiskTiles'\n"
	}

	if stb.RiskTilesOpen < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'RiskTilesOpen'\n"
	}

	if stb.MaxTrades < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'MaxTrades'\n"
	}

	if stb.MarginUtil < 0.01 || stb.MarginUtil > 0.99 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'MarginUtil'\n"
	}

	if _, err := time.ParseDuration(stb.Heartbeat); err != nil {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'Heartbeat'\n"
	}

	if _, ok := v20.CandleGranDurMap[stb.TrendGran]; !ok {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'TrendGran'\n"
	}

	if errMsg != "" {
		validationErr = fmt.Errorf(errMsg)
	}

	return validationErr
}

//decides whether trend is up (1), down (-1), or sideways (0)
func (stb *strategyTrailBlazer) decide(trendCandles []v20.Candlestick) int {
	//output
	var decision int

	//first open and last close of trend
	openPrice, _ := strconv.ParseFloat(trendCandles[0].Mid.O, 64)
	closePrice, _ := strconv.ParseFloat(trendCandles[len(trendCandles)-1].Mid.C, 64)

	//length of trend
	trendLength := int(math.Trunc(math.Abs(closePrice-openPrice) * 10000))

	//set trendDirection
	var trendDirection int
	if closePrice > openPrice {
		trendDirection = 1
	} else {
		if closePrice < openPrice {
			trendDirection = -1
		} else {
			trendDirection = 0
		}
	}

	//decision time; 3 tile minimum
	if trendLength >= stb.TileSize*stb.SignalTiles {
		decision = trendDirection
	}

	if checkLogLevel(1) {
		log.Printf("trendLength: %d, decision: %d\n", trendLength, decision)
	}

	return decision
}

//execute does trade actions if necessary; returns true if successfully executed
func (stb *strategyTrailBlazer) execute(decision int, instrument string, openTrades []v20.Trade) bool {
	if checkLogLevel(3) {
		log.Printf("inside execute! current decision: %d\n", decision)
	}

	//exit if decision is 0
	if decision == 0 {
		return true
	}

	//ensure current position is not already open
	if len(openTrades) > 0 {
		for _, trade := range openTrades {
			if trade.Instrument == instrument {
				if checkLogLevel(2) {
					log.Printf("no action taken; open trade found involving %s", instrument)
				}
				return true
			}
		}
	}

	//Get account info
	account, _ := v20.GetAccount()
	balance, _ := strconv.ParseFloat(account.Balance, 64)
	marginAvail, _ := strconv.ParseFloat(account.MarginAvailable, 64)
	marginRate, _ := strconv.ParseFloat(account.MarginRate, 64)

	//get margin to utilize for this trade
	tradeMarginUtil := balance * stb.MarginUtil / float64(stb.MaxTrades)

	//if available margin is less than tradeMarginUtil, base util off available
	if marginAvail < tradeMarginUtil {
		log.Printf("insufficient available margin; wanted %f, ", tradeMarginUtil)
		tradeMarginUtil = marginAvail * stb.MarginUtil
		log.Printf("now trading with %f", tradeMarginUtil)
	}

	//get instrument price

	//set up request for recent candlestick..
	priceCandleReq := v20.CandlestickGetReqWrapper{
		Instrument:  instrument,
		Price:       "BAM",
		Granularity: stb.TrendGran,
		Count:       1,
		//Smooth:      true,
		//Print:       true,
	}
	priceCandleResponse, err := v20.GetCandlesticksResponse(priceCandleReq)
	if err != nil {
		log.Printf("error occurred getting price candlestick for instrument %s", instrument)
		return false
	}

	priceCandle := priceCandleResponse.Candles[0]

	priceCloseInstrumentA, _ := strconv.ParseFloat(priceCandle.Ask.C, 64)
	priceCloseInstrumentB, _ := strconv.ParseFloat(priceCandle.Bid.C, 64)
	priceInstrumentM, _ := strconv.ParseFloat(priceCandle.Mid.C, 64)
	if checkLogLevel(2) {
		log.Printf("current price for instrument %s is %f", instrument, priceInstrumentM)
	}

	//trade sizing
	var units int
	//if account currency and instrument numerator are same
	if account.Currency == instrument[:3] {
		units = int(math.Trunc(tradeMarginUtil/marginRate/100)) * 100
	} else {
		if account.Currency == instrument[len(instrument)-3:] {
			units = int(math.Trunc(tradeMarginUtil/marginRate/priceInstrumentM/100)) * 100
		} else {
			log.Printf("error: this strategy can only trade instruments that contain account base currency [%s] but instrument provided is %s", account.Currency, instrument)
			return false
		}
	}

	//Make units same direction as decision
	directionName := "LONG"
	var priceSL float64
	if decision < 0 {
		units = -units
		directionName = "SHORT"
		priceSL = priceCloseInstrumentA + float64(stb.RiskTilesOpen*stb.TileSize)*v20.PIP
	} else {
		priceSL = priceCloseInstrumentB - float64(stb.RiskTilesOpen*stb.TileSize)*v20.PIP
	}

	if checkLogLevel(2) {
		log.Printf("units of %s to trade: %d\n", instrument, units)
	}

	//Execute market order w/SL
	newTradeID := v20.OpenNewMarketWStopLossPriceOrder(instrument, units, priceSL)
	if newTradeID == "-1" {
		log.Printf("error: could not execute v20 order\n")
		return false
	}

	log.Printf("opened new %s position! >> TradeID: %s >> %d units of %s\n", directionName, newTradeID, units, instrument)

	//log new trade
	if checkLogLevel(1) {
		//  Get open trades
		openTrades, err := v20.GetOpenTrades()
		if err != nil {
			log.Println("error: couldn't get open trades after opening new position")
			return false
		}
		for _, trade := range openTrades {
			if trade.ID == newTradeID {
				log.Println("\n", trade.String())
			}
		}
	}

	return true
}

//gets candlesticks from beginning of trend that led to activeTrade, through current candle
func (stb *strategyTrailBlazer) getActiveTradeCandles(activeTrade v20.Trade) []v20.Candlestick {
	if checkLogLevel(3) {
		log.Printf("inside getActiveTradeCandles..")
	}

	//output
	activeTradeCandles := []v20.Candlestick{}

	if checkLogLevel(2) {
		log.Printf("getActiveTradeCandles >> timeFrom: %s", activeTrade.OpenTime)
	}

	//set up request(s) for activeTradeCandles..
	tradeCandleReq := v20.CandlestickGetReqWrapper{
		Instrument:   activeTrade.Instrument,
		From:         activeTrade.OpenTime,
		IncludeFirst: true,
		Granularity:  stb.TrendGran,
		Price:        "BAM",
		//Smooth:      true,
		Print: true,
	}
	tradeCandleResponse, err := v20.GetCandlesticksResponse(tradeCandleReq)
	if err != nil || len(tradeCandleResponse.Candles) == 0 {
		return activeTradeCandles
	}

	activeTradeCandles = tradeCandleResponse.Candles

	if checkLogLevel(3) {
		for i, candle := range activeTradeCandles {
			log.Printf("Active Trade Candle #%d:\n%s\n", i+1, candle.String())
		}
	}
	return activeTradeCandles
}

//gets set of candlesticks making up an active trend
func (stb *strategyTrailBlazer) getActiveTrendCandles(then time.Time, currency string) []v20.Candlestick {
	if checkLogLevel(3) {
		log.Printf("inside getActiveTrendCandles..")
	}

	//output
	activeTrendCandles := []v20.Candlestick{}

	//direction of trend {1: LONG, 0:NIL, -1:SHORT}
	var trendDirection int

	//get recent candlesticks
	timeTo := v20.LocalToCandleTime(then.Add(-(v20.CandleGranDurMap[stb.TrendGran])))

	if checkLogLevel(3) {
		log.Printf("getActiveTrendCandles >> timeTo: %s", timeTo)
	}

	trendCandleReq := v20.CandlestickGetReqWrapper{
		Instrument:     currency,
		To:             timeTo,
		Price:          "M",
		Granularity:    stb.TrendGran,
		DailyAlignment: -1,
		//Print:       true,
	}
	trendCandleResponse, err := v20.GetCandlesticksResponse(trendCandleReq)
	if err != nil || len(trendCandleResponse.Candles) == 0 {
		return activeTrendCandles
	}

	recentCandles := trendCandleResponse.Candles

	//add most recent completed candle and any further on-trend candles in chrono order
	for i := len(recentCandles) - 1; i >= 0; i-- {
		//if first candle in activeTrendCandles, add with no further conditions and set trendDirection
		openPrice, _ := strconv.ParseFloat(recentCandles[i].Mid.O, 64)
		closePrice, _ := strconv.ParseFloat(recentCandles[i].Mid.C, 64)
		if len(activeTrendCandles) == 0 {
			//set trendDirection
			if closePrice > openPrice {
				trendDirection = 1
				activeTrendCandles = append(activeTrendCandles, recentCandles[i])
			} else {
				if closePrice < openPrice {
					trendDirection = -1
					activeTrendCandles = append(activeTrendCandles, recentCandles[i])
				} else {
					//if trend 0, can't start trend
					continue
				}
			}
		} else {
			//if candle is opposing trend, end of trend has been reached
			if closePrice > openPrice && trendDirection == -1 || closePrice < openPrice && trendDirection == 1 {
				break
			} else {
				//current candle is on-trend or zero body
				activeTrendCandles = append(activeTrendCandles, recentCandles[i])
			}
		}
	}

	activeTrendCandleCount := len(activeTrendCandles)
	if activeTrendCandleCount > 1 {
		//reverse activeTrendCandles
		for i := activeTrendCandleCount - 1; i >= 0; i-- {
			activeTrendCandles = append(activeTrendCandles, activeTrendCandles[i])

			if checkLogLevel(2) {
				log.Printf("Trend Candle #%d:\n%s\n", activeTrendCandleCount-i, activeTrendCandles[i].String())
			}
		}
		activeTrendCandles = activeTrendCandles[len(activeTrendCandles)/2:]
	} else {
		if checkLogLevel(2) {
			log.Printf("Trend Candle #1:\n%s\n", activeTrendCandles[0].String())
		}
	}

	return activeTrendCandles
}

func (stb *strategyTrailBlazer) tradeUpkeep(trade v20.Trade, activeTradeCandles []v20.Candlestick) bool {
	//perform regular per-candlestick upkeep

	if checkLogLevel(3) {
		log.Println("inside tradeUpkeep..")
	}

	//get tradeCurrentUnits for trade direction
	tradeCurrentUnits, _ := strconv.ParseFloat(trade.CurrentUnits, 64)

	//get entry price for trade
	entryPrice, _ := strconv.ParseFloat(trade.Price, 64)

	priceCandle := activeTradeCandles[len(activeTradeCandles)-1]

	priceInstrumentB, _ := strconv.ParseFloat(priceCandle.Bid.C, 64)
	priceInstrumentA, _ := strconv.ParseFloat(priceCandle.Ask.C, 64)

	distanceSpread := priceInstrumentA - priceInstrumentB

	if checkLogLevel(2) {
		log.Printf("tradeUpkeep: current spread: %f\n", distanceSpread)
		log.Printf("tradeUpkeep: active trade candles: %d\n", len(activeTradeCandles))
	}

	//process all trade candles to determine current maxTradeProfitPrice
	//adjust S/L order if latest (final) candle sets new max
	//price of max profit, starts as entry price
	var candle v20.Candlestick
	maxTradeProfitPrice := entryPrice
	for i := 0; i < len(activeTradeCandles); i++ {
		candle = activeTradeCandles[i]
		var maxCandleProfitPrice float64
		if tradeCurrentUnits > 0 {
			maxCandleProfitPrice, _ = strconv.ParseFloat(candle.Bid.H, 64)
		} else {
			maxCandleProfitPrice, _ = strconv.ParseFloat(candle.Ask.L, 64)
		}

		if tradeCurrentUnits > 0 && maxCandleProfitPrice > maxTradeProfitPrice || tradeCurrentUnits < 0 && maxCandleProfitPrice < maxTradeProfitPrice {
			//update new max profit price
			maxTradeProfitPrice = maxCandleProfitPrice
		}
	}

	//if StopLossOrder exists, get current order price
	var newPriceSL, currentPriceSL float64
	if trade.StopLossOrder != nil {
		currentPriceSL, _ = strconv.ParseFloat(trade.StopLossOrder.Price, 64)
		newPriceSL = currentPriceSL
	}

	//if a new max was made, update S/L
	if maxTradeProfitPrice != entryPrice || trade.StopLossOrder == nil {
		if checkLogLevel(1) {
			log.Printf("tradeUpkeep: new maxTradeProfitPrice: %f\n", maxTradeProfitPrice)
		}

		if tradeCurrentUnits > 0 {
			tradeProgressTiles := int(math.Trunc((maxTradeProfitPrice - entryPrice) / (float64(stb.TileSize) / float64(10000))))
			if tradeProgressTiles < 0 {
				tradeProgressTiles = 0
			}

			if checkLogLevel(1) {
				log.Printf("tradeUpkeep: tradeProgressTiles: %d\n", tradeProgressTiles)
			}

			if tradeProgressTiles >= 1 && tradeProgressTiles < 5 {
				//keep 40% of tradeProgressTiles
				keepProg := 0.4
				newPriceSL = entryPrice + float64(tradeProgressTiles*stb.TileSize)*keepProg*v20.PIP
			} else {
				//set newPriceSL to riskTiles back based on maxTradeProfitPrice
				newPriceSL = maxTradeProfitPrice - float64(stb.RiskTiles*stb.TileSize)*v20.PIP
			}

			//ensure S/L is always improving position
			if trade.StopLossOrder != nil && newPriceSL < currentPriceSL {
				newPriceSL = currentPriceSL
			}
		} else {
			tradeProgressTiles := int(math.Trunc((entryPrice - maxTradeProfitPrice) / (float64(stb.TileSize) / float64(10000))))
			if tradeProgressTiles < 0 {
				tradeProgressTiles = 0
			}

			if checkLogLevel(1) {
				log.Printf("tradeUpkeep: tradeProgressTiles: %d\n", tradeProgressTiles)
			}

			if tradeProgressTiles >= 1 && tradeProgressTiles < 5 {
				//keep 40% of progress
				keepProg := 0.4
				newPriceSL = entryPrice - float64(tradeProgressTiles*stb.TileSize)*keepProg*v20.PIP
			} else {
				//set newPriceSL to riskTiles back based on maxTradeProfitPrice
				newPriceSL = maxTradeProfitPrice + float64(stb.RiskTiles*stb.TileSize)*v20.PIP
			}

			//ensure S/L is always improving position
			if trade.StopLossOrder != nil && newPriceSL > currentPriceSL {
				newPriceSL = currentPriceSL
			}
		}
	}

	//perform upkeep
	//  S/L
	//    if StopLossOrder exists
	if trade.StopLossOrder != nil {
		//if newPriceSL is BETTER than currentPriceSL, they'll be different
		if newPriceSL != currentPriceSL {
			if checkLogLevel(1) {
				log.Printf("tradeUpkeep: newPriceSL: %f\n", newPriceSL)
			}
			v20.PutTradeOrder(v20.OrderReqWrapper{
				OrderID: trade.StopLossOrder.ID,
				TradeID: trade.ID,
				Type:    "STOP_LOSS",
				PriceSL: newPriceSL,
			})
		} else {
			log.Printf("tradeUpkeep: no upkeep performed for %s as SL price is unchanged", trade.Instrument)
			return true
		}
	} else {
		//there is no current S/L order so create one
		if checkLogLevel(1) {
			log.Printf("tradeUpkeep: newPriceSL: %f\n", newPriceSL)
		}
		v20.PostNewOrder(v20.OrderReqWrapper{
			TradeID:      trade.ID,
			Type:         "STOP_LOSS",
			PriceSL:      newPriceSL,
			PositionFill: "DEFAULT",
		})
	}

	//update T/P
	/*v20.PutTradeOrder(v20.OrderReqWrapper{
		OrderID: trade.TakeProfitOrder.ID,
		TradeID: trade.ID,
		Type:    "TAKE_PROFIT",
		PriceTP: newPriceTP,
	})*/

	//return true if upkeep is performed successfully
	return true
}

type strategyTrailBlazerResult struct {
	name   string
	isOK   bool
	rating int
	status int
	msg    string
	err    error
}

//OK returns overall boolean whether strategy was executed successfully or not
func (stbr *strategyTrailBlazerResult) OK() bool {
	return stbr.isOK
}

//String returns string representation of this result
func (stbr *strategyTrailBlazerResult) String() string {
	return fmt.Sprintf("\nstrategyTrailBlazerResult\n=========================\nname: %s\nisOK: %v\nrating: %d\nstatus: %d\nmsg: %s\nerr: %v", stbr.name, stbr.isOK, stbr.rating, stbr.status, stbr.msg, stbr.err)
}
