package portfolio

import (
	"fmt"

	stips "gitlab.com/tkz/lyTer/stipulations"
)

//action represents an action to be executed within a strategy
type action interface {
	decide() stips.StipResult
	String() string
}

type actionTrade struct {
	name     string
	category string
	stips    []stips.Stip
	decision stips.StipResult
	perform  func() actionResult //perform the actual action
	result   actionResult        //result of this action
}

//decide gets stipResult from each stip and assigns 'decision'
func (ta *actionTrade) decide() stips.StipResult {
	//get []stipResult; 1 result from each stip in the action
	results := ta.eval()

	//decision logic on stip results to get decision
	decision := results[0]

	//assign decision
	ta.decision = decision

	return ta.decision
}

func (ta *actionTrade) eval() []stips.StipResult {
	return []stips.StipResult{&stips.TradeStipResult{}}
}

type actionResult interface {
	String() string
}

type actionTradeResult struct {
	name   string
	isOK   bool
	rating int
	status int
	msg    string
	err    error
}

func (tar *actionTradeResult) String() string {
	return fmt.Sprintf("\nactionTradeResult\n============\nname: %s\nisOK: %v\nrating: %d\nstatus: %d\nmsg: %s\nerr: %v", tar.name, tar.isOK, tar.rating, tar.status, tar.msg, tar.err)
}
