package portfolio

import (
	"fmt"
)

type stageResult struct {
	name    string
	isOK    bool
	rating  int
	status  int
	msg     string
	doNext  bool
	doIndex int
	err     error
}

//OK returns overall boolean whether stage was executed successfully or not
func (sr *stageResult) OK() bool {
	return sr.isOK
}

//String returns string representation of this result
func (sr *stageResult) String() string {
	return fmt.Sprintf("\nstageResult\n================\nname: %s\nisOK: %v\nrating: %d\nstatus: %d\nmsg: %s\ndoNext: %v\ndoIndex: %d\nerr: %v", sr.name, sr.isOK, sr.rating, sr.status, sr.msg, sr.doNext, sr.doIndex, sr.err)
}
