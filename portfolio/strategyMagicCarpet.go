package portfolio

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"time"

	"gitlab.com/tkz/lyTer/v20"
)

type strategyMagicCarpet struct {
	Name            string         `json:"name,omitempty"`
	Heartbeat       string         `json:"heartbeat,omitempty"`
	TrendGran       string         `json:"trendGran,omitempty"`
	TileSize        int            `json:"tileSize,omitempty"`
	SignalTiles     int            `json:"signalTiles,omitempty"`
	RiskTiles       int            `json:"riskTiles,omitempty"`
	MaxTrades       int            `json:"maxTrades,omitempty"`
	MaxTrendAge     string         `json:"maxTrendAge,omitempty"`
	MarginUtil      float64        `json:"marginUtil,omitempty"`
	IsCandleAligned bool           `json:"isCandleAligned,omitempty"`
	Result          StrategyResult `json:"-"`
}

func (smc *strategyMagicCarpet) Run(currencies []string) StrategyResult {
	//assign initial result
	result := StrategyResult{name: smc.Name, isOK: true}

	//=====
	//BEGIN - TrailBlazer Strategy Flow
	//=====

	if checkLogLevel(3) {
		log.Println("inside MagicCarpet Run!")
	}

	if len(currencies) < 1 {
		result.isOK = false
		result.msg = "error: no currencies provided to Run([]string) call"
		return result
	}

	//Get open trades
	openTrades, err := v20.GetOpenTrades()
	if err != nil {
		result.isOK = false
		return result
	}
	openTradeCount := len(openTrades)

	//tradeUpkeep if openTrades
	if checkLogLevel(1) {
		log.Printf("openTradeCount: %d", openTradeCount)
	}
	if openTradeCount > 0 {
		//update each trade
		for i, trade := range openTrades {
			if checkLogLevel(2) {
				log.Printf("#%d:\n\n%s", i+1, trade.String())
			}

			tradeCandles := smc.getActiveTradeCandles(trade, false)

			//adjust/add  S/L order if necessary
			result.isOK = smc.tradeUpkeep(trade, tradeCandles)
		}
	}

	//filter availableCurrencies by openTrades
	var availableCurrencies []string
	if openTradeCount > 0 {
		if openTradeCount < smc.MaxTrades {
			availableCurrencies = []string{}
			for _, currency := range currencies {
				var foundCurrency bool
				for _, trade := range openTrades {
					if trade.Instrument == currency {
						foundCurrency = true
						break
					}
				}
				if !foundCurrency {
					availableCurrencies = append(availableCurrencies, currency)
				}
			}
		} else {
			if checkLogLevel(1) {
				log.Printf("open trade maximum reached: %d\n", smc.MaxTrades)
			}
			return result
		}
	} else {
		availableCurrencies = currencies
	}

	if checkLogLevel(2) {
		log.Printf("availableCurrencies: %v", availableCurrencies)
	}

	if len(availableCurrencies) < 1 {
		result.msg = "skip open new positions: no availableCurrencies"
		return result
	}

	//set up request for current (trend) candle start time
	currentCandleReq := v20.CandlestickGetReqWrapper{
		Instrument:  availableCurrencies[0],
		Price:       "M",
		Granularity: smc.TrendGran,
		Count:       1,
		//Smooth:      true,
		//Print:       true,
	}
	currentCandleResponse, err := v20.GetCandlesticksResponse(currentCandleReq)
	if err != nil || len(currentCandleResponse.Candles) == 0 {
		log.Printf("error: could not retrieve current candle\n")
		result.isOK = false
		return result
	}

	currentCandle := currentCandleResponse.Candles[0]

	timeStartCurrentCandle := currentCandle.Time

	//skip opening new positions if now is passed MaxTrendAge
	localTimeStartCurrentCandle := v20.CandleToLocalTime(timeStartCurrentCandle)

	if checkLogLevel(3) {
		log.Printf("currentCandle.Time: %v; localTimeStartCurrentCandle: %v", timeStartCurrentCandle, localTimeStartCurrentCandle)
	}

	//if time for candle has lapsed, go with next candle start time
	if now := getTimeNowEST(); now.Add(time.Second * 2).After(localTimeStartCurrentCandle.Add(v20.CandleGranDurMap[smc.TrendGran])) {
		localTimeStartCurrentCandle = localTimeStartCurrentCandle.Add(v20.CandleGranDurMap[smc.TrendGran])
	}

	if getTimeNowEST().After(localTimeStartCurrentCandle.Add(smc.GetMaxTrendAge())) {
		log.Printf("skip open new positions: trend older than MaxTrendAge of '%s'\n", smc.MaxTrendAge)
		return result
	}

	//conditional execution of new trade
	var activeTrendCandles []v20.Candlestick
	for currIdx := 0; currIdx < len(availableCurrencies) && openTradeCount < smc.MaxTrades; currIdx++ {
		//STRATEGY: ensure no ORDER_FILL transactions within current candle for current currency

		if checkLogLevel(1) {
			log.Printf("evaluating instrument '%s'..", availableCurrencies[currIdx])
		}

		activeTrendCandles = smc.getActiveTrendCandles(getTimeNowEST().Add(-time.Second*2), availableCurrencies[currIdx], false)
		//decide on execution
		if decision := smc.decide(activeTrendCandles); decision != 0 {
			//no execution if ORDER_FILL transactions found within last trend candle
			candleTransactions, err := v20.GetTransactionsSince(localTimeStartCurrentCandle.Add(-(time.Second * 5)), []string{"ORDER_FILL"})
			if err != nil {
				log.Printf("error: could not retrieve all requested transactions\n>>%v", err)
				result.isOK = false
				return result
			}

			//only consider transactions matching this currency
			matchingTransactions := []v20.Transaction{}
			for _, tx := range candleTransactions {
				if tx.Instrument == availableCurrencies[currIdx] {
					matchingTransactions = append(matchingTransactions, tx)
				}
			}

			if checkLogLevel(2) {
				log.Printf("transactions found: %d", len(matchingTransactions))
			}

			//if there are ORDER_FILL transactions within current trend candle, return
			if len(matchingTransactions) > 0 {
				//don't open new position yet
				if checkLogLevel(1) {
					log.Printf("%s: trade not opened >> existing ORDER_FILL transaction(s) found within current candlestick.", availableCurrencies[currIdx])
				}
				continue
			}

			//execute actions if necessary
			result.isOK = smc.execute(decision, availableCurrencies[currIdx], activeTrendCandles, openTrades)
			if result.isOK {
				openTradeCount++
			} else {
				result.msg = "could not execute trade successfully"
			}
		}
	}

	//assign/return result
	smc.Result = result
	return result
}

func (smc *strategyMagicCarpet) GetIsCandleAligned() bool {
	return smc.IsCandleAligned
}

func (smc *strategyMagicCarpet) GetHeartbeat() time.Duration {
	heartbeatDuration, err := time.ParseDuration(smc.Heartbeat)
	if err != nil {
		log.Panicf("smc heartbeat '%s' is not a valid duration string", smc.Heartbeat)
	}

	return heartbeatDuration
}

func (smc *strategyMagicCarpet) GetMaxTrendAge() time.Duration {
	maxTrendAgeDuration, err := time.ParseDuration(smc.MaxTrendAge)
	if err != nil {
		log.Panicf("smc maxTrendAge '%s' is not a valid duration string", smc.MaxTrendAge)
	}

	return maxTrendAgeDuration
}

func (smc *strategyMagicCarpet) GetName() string {
	return smc.Name
}

func (smc *strategyMagicCarpet) GetTrendGran() string {
	return smc.TrendGran
}

func (smc *strategyMagicCarpet) String() string {
	return fmt.Sprintf("Strategy MagicCarpet\n====================\nName: %s\nTileSize: %d\nSignalTiles: %d\nRiskTiles: %d\nMaxTrades: %d\nMaxTrendAge: %s\nMarginUtil: %f\nHeartbeat: %s\nTrendGran: %s\nIsCandleAligned: %v\n", smc.Name, smc.TileSize, smc.SignalTiles, smc.RiskTiles, smc.MaxTrades, smc.MaxTrendAge, smc.MarginUtil, smc.Heartbeat, smc.TrendGran, smc.IsCandleAligned)
}

func (smc *strategyMagicCarpet) Validate() error {
	//output
	var validationErr error

	//error message
	errMsg := ""

	if smc.Name == "" {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'Name'\n"
	}

	if smc.TileSize < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'TileSize'\n"
	}

	if smc.SignalTiles < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'SignalTiles'\n"
	}

	if smc.RiskTiles < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'RiskTiles'\n"
	}

	if smc.MaxTrades < 1 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'MaxTrades'\n"
	}

	if _, err := time.ParseDuration(smc.MaxTrendAge); err != nil || smc.GetMaxTrendAge() < 0 || smc.GetMaxTrendAge() >= v20.CandleGranDurMap[smc.TrendGran] {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'MaxTrendAge'; value must be positive and less than Trand candle duration\n"
	}

	if smc.MarginUtil < 0.01 || smc.MarginUtil > 0.99 {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'MarginUtil'\n"
	}

	if _, err := time.ParseDuration(smc.Heartbeat); err != nil {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'Heartbeat'\n"
	}

	if _, ok := v20.CandleGranDurMap[smc.TrendGran]; !ok {
		errMsg += "->ACTIVE_STRATEGY_CONFIG has invalid 'TrendGran'\n"
	}

	if errMsg != "" {
		validationErr = fmt.Errorf(errMsg)
	}

	return validationErr
}

//decides whether trend is up (1), down (-1), or sideways (0)
func (smc *strategyMagicCarpet) decide(trendCandles []v20.Candlestick) int {
	//output
	var decision int

	//if no candles, no decision
	if len(trendCandles) == 0 {
		if checkLogLevel(1) {
			log.Println("no valid trend candles retrieved; decision: 0")
		}
		return 0
	}

	//first open and last close of trend
	openPrice, _ := strconv.ParseFloat(trendCandles[0].Mid.O, 64)
	closePrice, _ := strconv.ParseFloat(trendCandles[len(trendCandles)-1].Mid.C, 64)

	//length of trend
	trendLength := int(math.Trunc(math.Abs(closePrice-openPrice) * 10000))

	//set trendDirection
	var trendDirection int
	if closePrice > openPrice {
		trendDirection = 1
	} else {
		if closePrice < openPrice {
			trendDirection = -1
		} else {
			trendDirection = 0
		}
	}

	//decision time; 3 tile minimum
	if trendLength >= smc.TileSize*smc.SignalTiles {
		decision = trendDirection
	}

	if checkLogLevel(1) {
		log.Printf("trendLength: %d, decision: %d\n", trendLength, decision)
	}

	return decision
}

//execute does trade actions if necessary; returns true if successfully executed
func (smc *strategyMagicCarpet) execute(decision int, instrument string, activeTrendCandles []v20.Candlestick, openTrades []v20.Trade) bool {
	if checkLogLevel(3) {
		log.Printf("inside execute! current decision: %d\n", decision)
	}

	//exit if decision is 0
	if decision == 0 {
		return true
	}

	//ensure current position is not already open
	if len(openTrades) > 0 {
		for _, trade := range openTrades {
			if trade.Instrument == instrument {
				if checkLogLevel(2) {
					log.Printf("no action taken; open trade found involving %s", instrument)
				}
				return true
			}
		}
	}

	//Get account info
	account, _ := v20.GetAccount()
	balance, _ := strconv.ParseFloat(account.Balance, 64)
	marginAvail, _ := strconv.ParseFloat(account.MarginAvailable, 64)
	marginRate, _ := strconv.ParseFloat(account.MarginRate, 64)

	//get margin to utilize for this trade
	tradeMarginUtil := balance * smc.MarginUtil / float64(smc.MaxTrades)

	//if available margin is less than tradeMarginUtil, base util off available
	if marginAvail < tradeMarginUtil {
		log.Printf("insufficient available margin; wanted %f, ", tradeMarginUtil)
		tradeMarginUtil = marginAvail * smc.MarginUtil
		log.Printf("now trading with %f", tradeMarginUtil)
	}

	//get latest trend candle close price
	priceCandle := activeTrendCandles[len(activeTrendCandles)-1]

	priceCloseInstrumentA, _ := strconv.ParseFloat(priceCandle.Ask.C, 64)
	priceCloseInstrumentB, _ := strconv.ParseFloat(priceCandle.Bid.C, 64)
	priceInstrumentM, _ := strconv.ParseFloat(priceCandle.Mid.C, 64)
	if checkLogLevel(2) {
		log.Printf("current price for instrument %s is %f", instrument, priceInstrumentM)
	}

	//trade sizing
	var units int
	//if account currency and instrument numerator are same
	if account.Currency == instrument[:3] {
		units = int(math.Trunc(tradeMarginUtil/marginRate/100)) * 100
	} else {
		if account.Currency == instrument[len(instrument)-3:] {
			units = int(math.Trunc(tradeMarginUtil/marginRate/priceInstrumentM/100)) * 100
		} else {
			log.Printf("error: this strategy can only trade instruments that contain account base currency [%s] but instrument provided is %s", account.Currency, instrument)
			return false
		}
	}

	//Make units same direction as decision
	directionName := "LONG"
	var priceSL float64
	if decision < 0 {
		units = -units
		directionName = "SHORT"
		priceSL = priceCloseInstrumentA + float64(smc.RiskTiles*smc.TileSize)*v20.PIP
	} else {
		priceSL = priceCloseInstrumentB - float64(smc.RiskTiles*smc.TileSize)*v20.PIP
	}

	if checkLogLevel(2) {
		log.Printf("units of %s to trade: %d\n", instrument, units)
	}

	//Execute market order w/SL
	newTradeID := v20.OpenNewMarketWStopLossPriceOrder(instrument, units, priceSL)
	if newTradeID == "-1" {
		log.Printf("error: could not execute v20 order\n")
		return false
	}

	log.Printf("opened new %s position! >> TradeID: %s >> %d units of %s\n", directionName, newTradeID, units, instrument)

	//log new trade
	if checkLogLevel(1) {
		//  Get open trades
		openTrades, err := v20.GetOpenTrades()
		if err != nil {
			log.Println("error: couldn't get open trades after opening new position")
			return false
		}
		for _, trade := range openTrades {
			if trade.ID == newTradeID {
				log.Println("\n", trade.String())
			}
		}
	}

	return true
}

//gets candlesticks from beginning of trend that led to activeTrade, through current candle
func (smc *strategyMagicCarpet) getActiveTradeCandles(activeTrade v20.Trade, includeCurrent bool) []v20.Candlestick {
	if checkLogLevel(3) {
		log.Printf("inside getActiveTradeCandles..")
	}

	//output
	validActiveTradeCandles := []v20.Candlestick{}

	//response candles
	activeTradeCandles := []v20.Candlestick{}

	if checkLogLevel(2) {
		log.Printf("getActiveTradeCandles >> timeFrom: %s; includeCurrent: %v\n", activeTrade.OpenTime, includeCurrent)
	}

	//set up request(s) for activeTradeCandles..
	tradeCandleReq := v20.CandlestickGetReqWrapper{
		Instrument:   activeTrade.Instrument,
		From:         activeTrade.OpenTime,
		IncludeFirst: true,
		Granularity:  smc.TrendGran,
		Price:        "BAM",
		//Smooth:      true,
		Print: true,
	}
	tradeCandleResponse, err := v20.GetCandlesticksResponse(tradeCandleReq)
	if err != nil || len(tradeCandleResponse.Candles) == 0 {
		return activeTradeCandles
	}

	activeTradeCandles = tradeCandleResponse.Candles

	for i, candle := range activeTradeCandles {
		//skip if not includeCurrent and candle is not entirely in the past
		if !includeCurrent {
			timeEndCandle := v20.CandleToLocalTime(candle.Time).Add(v20.CandleGranDurMap[smc.TrendGran])
			if now := getTimeNowEST(); !now.After(timeEndCandle.Add(time.Second * 2)) {
				continue
			}
		}

		validActiveTradeCandles = append(validActiveTradeCandles, candle)

		if checkLogLevel(3) {
			log.Printf("Active Trade Candle #%d:\n%s\n", i+1, candle.String())
		}
	}

	return validActiveTradeCandles
}

//gets set of candlesticks making up an active trend
func (smc *strategyMagicCarpet) getActiveTrendCandles(then time.Time, currency string, includeCurrent bool) []v20.Candlestick {
	if checkLogLevel(3) {
		log.Printf("inside getActiveTrendCandles..")
	}

	//output
	validActiveTrendCandles := []v20.Candlestick{}

	//running trend candles up until 'then'
	activeTrendCandles := []v20.Candlestick{}

	//direction of trend {1: LONG, 0:NIL, -1:SHORT}
	var trendDirection int

	//get candlesticks until 'then'
	if checkLogLevel(3) {
		log.Printf("getActiveTrendCandles >> then: %s; includeCurrent: %v", then, includeCurrent)
	}

	trendCandleReq := v20.CandlestickGetReqWrapper{
		Instrument:     currency,
		To:             v20.LocalToCandleTime(then),
		Price:          "BAM",
		Granularity:    smc.TrendGran,
		DailyAlignment: -1,
		//Print:       true,
	}
	trendCandleResponse, err := v20.GetCandlesticksResponse(trendCandleReq)
	if err != nil || len(trendCandleResponse.Candles) == 0 {
		return activeTrendCandles
	}

	recentCandles := trendCandleResponse.Candles

	//add most recent completed candle and any further on-trend candles in chrono order
	for i := len(recentCandles) - 1; i >= 0; i-- {
		//skip if not includeCurrent and candle is not entirely in the past
		if !includeCurrent {
			timeEndCandle := v20.CandleToLocalTime(recentCandles[i].Time).Add(v20.CandleGranDurMap[smc.TrendGran])
			if now := getTimeNowEST(); !now.Add(time.Second * 2).After(timeEndCandle) {
				continue
			}
		}

		//if first candle in activeTrendCandles, append and set trendDirection
		openPrice, _ := strconv.ParseFloat(recentCandles[i].Mid.O, 64)
		closePrice, _ := strconv.ParseFloat(recentCandles[i].Mid.C, 64)
		if len(activeTrendCandles) == 0 {
			//set trendDirection
			if closePrice > openPrice {
				trendDirection = 1
				activeTrendCandles = append(activeTrendCandles, recentCandles[i])
			} else {
				if closePrice < openPrice {
					trendDirection = -1
					activeTrendCandles = append(activeTrendCandles, recentCandles[i])
				} else {
					//if trend 0, can't start trend
					continue
				}
			}
		} else {
			//if candle is opposing trend, end of trend has been reached
			if closePrice > openPrice && trendDirection == -1 || closePrice < openPrice && trendDirection == 1 {
				break
			} else {
				//current candle is on-trend or zero body
				activeTrendCandles = append(activeTrendCandles, recentCandles[i])
			}
		}
	}

	//get most recent transaction within active trend for this currency
	localTimeStartFirstTrendCandle := v20.CandleToLocalTime(activeTrendCandles[len(activeTrendCandles)-1].Time)

	candleTransactions, err := v20.GetOrderFillTransactionsSince(localTimeStartFirstTrendCandle, []string{"ORDER_FILL"})
	if err != nil {
		log.Printf("error: could not retrieve all requested transactions\n>>%v", err)
		return validActiveTrendCandles
	}

	//get time of latest transaction within trend of this currency
	var timeLatestTrendTx string
	if len(candleTransactions) > 0 {
		for i := len(candleTransactions) - 1; i >= 0; i-- {
			if candleTransactions[i].Instrument == currency {
				timeLatestTrendTx = candleTransactions[i].Time
				break
			}
		}
	}

	var localTimeLatestTrendTx time.Time
	if timeLatestTrendTx != "" {
		localTimeLatestTrendTx = v20.CandleToLocalTime(timeLatestTrendTx)
	}

	//activeTrendCandles are ordered w/most recent first, reverse and filter out candles w/txs and older
	activeTrendCandleCount := len(activeTrendCandles)
	for i := activeTrendCandleCount - 1; i >= 0; i-- {
		if timeLatestTrendTx == "" {
			//insert first to reverse order; will now be oldest candle first
			validActiveTrendCandles = append(validActiveTrendCandles, activeTrendCandles[i])
			if checkLogLevel(2) {
				log.Printf("Trend Candle #%d:\n%s\n", activeTrendCandleCount-i, activeTrendCandles[i].String())
			}
		} else {
			localCandleStartTime := v20.CandleToLocalTime(activeTrendCandles[i].Time)
			if localCandleStartTime.After(localTimeLatestTrendTx) {
				//insert first to reverse order; will now be oldest candle first
				validActiveTrendCandles = append([]v20.Candlestick{activeTrendCandles[i]}, validActiveTrendCandles...)
				if checkLogLevel(2) {
					log.Printf("Trend Candle #%d:\n%s\n", activeTrendCandleCount-i, activeTrendCandles[i].String())
				}
			}
		}
	}

	return validActiveTrendCandles
}

func (smc *strategyMagicCarpet) tradeUpkeep(trade v20.Trade, activeTradeCandles []v20.Candlestick) bool {
	//perform regular per-candlestick upkeep

	if checkLogLevel(3) {
		log.Println("inside tradeUpkeep..")
	}

	//skip upkeep if no trade candles
	if len(activeTradeCandles) == 0 {
		log.Printf("tradeUpkeep: no upkeep performed for %s as no activeTradeCandles receieved", trade.Instrument)
		return true
	}

	//get tradeCurrentUnits for trade direction
	tradeCurrentUnits, _ := strconv.ParseFloat(trade.CurrentUnits, 64)

	//get entry price for trade
	entryPrice, _ := strconv.ParseFloat(trade.Price, 64)

	priceCandle := activeTradeCandles[len(activeTradeCandles)-1]

	priceInstrumentB, _ := strconv.ParseFloat(priceCandle.Bid.C, 64)
	priceInstrumentA, _ := strconv.ParseFloat(priceCandle.Ask.C, 64)

	distanceSpread := priceInstrumentA - priceInstrumentB

	if checkLogLevel(2) {
		log.Printf("tradeUpkeep: current spread: %f\n", distanceSpread)
		log.Printf("tradeUpkeep: active trade candles: %d\n", len(activeTradeCandles))
	}

	//process all trade candles to determine current maxProfitClosePrice
	//adjust S/L order if new max profit closing price has been reached
	maxProfitClosePrice := entryPrice
	var candle v20.Candlestick
	for i := 0; i < len(activeTradeCandles); i++ {
		candle = activeTradeCandles[i]
		var candleClosePrice float64
		if tradeCurrentUnits > 0 {
			candleClosePrice, _ = strconv.ParseFloat(candle.Bid.C, 64)
		} else {
			candleClosePrice, _ = strconv.ParseFloat(candle.Ask.C, 64)
		}

		if tradeCurrentUnits > 0 && candleClosePrice > maxProfitClosePrice || tradeCurrentUnits < 0 && candleClosePrice < maxProfitClosePrice {
			//update new max profit price
			maxProfitClosePrice = candleClosePrice
		}
	}

	//if StopLossOrder exists, get current order price
	var newPriceSL, currentPriceSL float64
	if trade.StopLossOrder != nil {
		currentPriceSL, _ = strconv.ParseFloat(trade.StopLossOrder.Price, 64)
		newPriceSL = currentPriceSL
	}

	//if a new max was made, update S/L
	if math.Abs(maxProfitClosePrice-entryPrice) >= v20.PIP*0.1 || trade.StopLossOrder == nil {
		if tradeCurrentUnits > 0 {
			newPriceSL = maxProfitClosePrice - float64(smc.RiskTiles*smc.TileSize)*v20.PIP

			//ensure S/L is always improving position
			if trade.StopLossOrder != nil && newPriceSL < currentPriceSL {
				newPriceSL = currentPriceSL
			} else {
				if checkLogLevel(1) {
					log.Printf("tradeUpkeep: new maxProfitClosePrice: %f\n", maxProfitClosePrice)
				}
			}
		} else {
			newPriceSL = maxProfitClosePrice + float64(smc.RiskTiles*smc.TileSize)*v20.PIP

			//ensure S/L is always improving position
			if trade.StopLossOrder != nil && newPriceSL > currentPriceSL {
				newPriceSL = currentPriceSL
			} else {
				if checkLogLevel(1) {
					log.Printf("tradeUpkeep: new maxProfitClosePrice: %f\n", maxProfitClosePrice)
				}
			}
		}
	}

	//perform upkeep
	//  S/L
	//    if StopLossOrder exists
	if trade.StopLossOrder != nil {
		//if newPriceSL is BETTER than currentPriceSL, they'll be different
		if math.Abs(newPriceSL-currentPriceSL) >= v20.PIP*0.1 {
			if checkLogLevel(1) {
				log.Printf("tradeUpkeep: newPriceSL: %f\n", newPriceSL)
			}
			v20.PutTradeOrder(v20.OrderReqWrapper{
				OrderID: trade.StopLossOrder.ID,
				TradeID: trade.ID,
				Type:    "STOP_LOSS",
				PriceSL: newPriceSL,
			})
		} else {
			log.Printf("tradeUpkeep: no upkeep performed for %s as SL price is unchanged", trade.Instrument)
			return true
		}
	} else {
		//there is no current S/L order so create one
		if checkLogLevel(1) {
			log.Printf("tradeUpkeep: newPriceSL: %f\n", newPriceSL)
		}
		v20.PostNewOrder(v20.OrderReqWrapper{
			TradeID:      trade.ID,
			Type:         "STOP_LOSS",
			PriceSL:      newPriceSL,
			PositionFill: "DEFAULT",
		})
	}

	//return true if upkeep is performed successfully
	return true
}

type strategyMagicCarpetResult struct {
	name   string
	isOK   bool
	rating int
	status int
	msg    string
	err    error
}

//OK returns overall boolean whether strategy was executed successfully or not
func (stbr *strategyMagicCarpetResult) OK() bool {
	return stbr.isOK
}

//String returns string representation of this result
func (stbr *strategyMagicCarpetResult) String() string {
	return fmt.Sprintf("\nstrategyMagicCarpetResult\n=========================\nname: %s\nisOK: %v\nrating: %d\nstatus: %d\nmsg: %s\nerr: %v", stbr.name, stbr.isOK, stbr.rating, stbr.status, stbr.msg, stbr.err)
}
