package v20

//ResponseGetTrade is the response struct for GET open trades
type ResponseGetTrade struct {

	// The list of open Trades in the Account.
	Trade Trade `json:"trade,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}
