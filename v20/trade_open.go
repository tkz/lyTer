/*
 * OANDA v20 REST API
 *
 * The full OANDA v20 REST API Specification. This specification defines how to interact with v20 Accounts, Trades, Orders, Pricing and more.
 *
 * API version: 3.0.22
 * Contact: api@oanda.com
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package v20

// A TradeOpen object represents a Trade for an instrument that was opened in an Account. It is found embedded in Transactions that affect the position of an instrument in the Account, specifically the OrderFill Transaction.
type TradeOpen struct {

	// The ID of the Trade that was opened
	TradeID string `json:"tradeID,omitempty"`

	// The number of units opened by the Trade
	Units string `json:"units,omitempty"`

	// The average price that the units were opened at.
	Price string `json:"price,omitempty"`

	// This is the fee charged for opening the trade if it has a guaranteed Stop Loss Order attached to it.
	GuaranteedExecutionFee string `json:"guaranteedExecutionFee,omitempty"`

	ClientExtensions *ClientExtensions `json:"clientExtensions,omitempty"`

	// The half spread cost for the trade open. This can be a positive or negative value and is represented in the home currency of the Account.
	HalfSpreadCost string `json:"halfSpreadCost,omitempty"`

	// The margin required at the time the Trade was created. Note, this is the 'pure' margin required, it is not the 'effective' margin used that factors in the trade risk if a GSLO is attached to the trade.
	InitialMarginRequired string `json:"initialMarginRequired,omitempty"`
}
