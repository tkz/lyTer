//Package v20 contains all v20 specific behavior
package v20

// ResponseGetAccount represents response to account GET request
type ResponseGetAccount struct {
	Account *Account `json:"account,omitempty"`

	// The ID of the most recent Transaction created for the Account.
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}
