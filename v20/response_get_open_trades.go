package v20

//ResponseGetOpenTrades is the response struct for GET open trades
type ResponseGetOpenTrades struct {

	// The list of open Trades in the Account.
	Trades []Trade `json:"trades,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}
