//Package v20 contains all v20 specific behavior
package v20

// ResponseGetCandles represents response to candles GET request
type ResponseGetCandles struct {

	// The instrument whose Prices are represented by the candlesticks.
	Instrument string `json:"instrument,omitempty"`

	// The granularity of the candlesticks provided.
	Granularity string `json:"granularity,omitempty"`

	// The list of candlesticks that satisfy the request.
	Candles []Candlestick `json:"candles,omitempty"`
}
