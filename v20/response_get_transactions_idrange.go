package v20

//ResponseGetTransactionsIDRange is the response struct for GET transactions/idrange
type ResponseGetTransactionsIDRange struct {

	// The list of transactions found from query
	Transactions []Transaction `json:"transactions,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}
