package v20

//ResponseGetOpenPositions is the response struct for GET open positions
type ResponseGetOpenPositions struct {

	// The list of open Positions in the Account.
	Positions []Position `json:"positions,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}
