package v20

//ResponsePostOrder is the 201 response struct for POST order
type ResponsePostOrder struct {
	OrderCreateTransaction *Transaction `json:"orderCreateTransaction,omitempty"`

	OrderFillTransaction *OrderFillTransaction `json:"orderFillTransaction,omitempty"`

	OrderCancelTransaction *OrderCancelTransaction `json:"orderCancelTransaction,omitempty"`

	OrderReissueTransaction *Transaction `json:"orderReissueTransaction,omitempty"`

	OrderReissueRejectTransaction *Transaction `json:"orderReissueRejectTransaction,omitempty"`

	// The IDs of all Transactions that were created while satisfying the request.
	RelatedTransactionIDs []string `json:"relatedTransactionIDs,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}

//ResponsePostOrder400 is the 400 response struct for POST order
type ResponsePostOrder400 struct {
	OrderRejectTransaction *Transaction `json:"orderRejectTransaction,omitempty"`

	// The IDs of all Transactions that were created while satisfying the request.
	RelatedTransactionIDs []string `json:"relatedTransactionIDs,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`

	// The code of the error that has occurred. This field may not be returned for some errors.
	ErrorCode string `json:"errorCode,omitempty"`

	// The human-readable description of the error that has occurred.
	ErrorMessage string `json:"errorMessage,omitempty"`
}

//ResponsePostOrder404 is the 404 response struct for POST order
type ResponsePostOrder404 struct {
	OrderRejectTransaction *Transaction `json:"orderRejectTransaction,omitempty"`

	// The ID of the most recent Transaction created for the Account. Only present if the Account exists.
	LastTransactionID string `json:"lastTransactionID,omitempty"`

	// The IDs of all Transactions that were created while satisfying the request. Only present if the Account exists.
	RelatedTransactionIDs []string `json:"relatedTransactionIDs,omitempty"`

	// The code of the error that has occurred. This field may not be returned for some errors.
	ErrorCode string `json:"errorCode,omitempty"`

	// The human-readable description of the error that has occurred.
	ErrorMessage string `json:"errorMessage,omitempty"`
}
