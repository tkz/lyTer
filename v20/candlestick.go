/*
 * OANDA v20 REST API
 *
 * The full OANDA v20 REST API Specification. This specification defines how to interact with v20 Accounts, Trades, Orders, Pricing and more.
 *
 * API version: 3.0.22
 * Contact: api@oanda.com
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package v20

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"strconv"
	"time"
)

var (
	//CandleGranDurMap is a map of candlestick granularities to durations of time
	CandleGranDurMap = map[string]time.Duration{
		"S5":  time.Second * 5,
		"S10": time.Second * 10,
		"S15": time.Second * 15,
		"S30": time.Second * 30,
		"M1":  time.Minute * 1,
		"M2":  time.Minute * 2,
		"M4":  time.Minute * 4,
		"M5":  time.Minute * 5,
		"M10": time.Minute * 10,
		"M15": time.Minute * 15,
		"M30": time.Minute * 30,
		"H1":  time.Hour * 1,
		"H2":  time.Hour * 2,
		"H3":  time.Hour * 3,
		"H4":  time.Hour * 4,
		"H6":  time.Hour * 6,
		"H8":  time.Hour * 8,
		"H12": time.Hour * 12,
		"D":   time.Hour * 24,
		"W":   time.Hour * 24 * 7,
	}
)

//Candlestick representation
type Candlestick struct {

	// The start time of the candlestick
	Time string `json:"time,omitempty"`

	Bid *CandlestickData `json:"bid,omitempty"`

	Ask *CandlestickData `json:"ask,omitempty"`

	Mid *CandlestickData `json:"mid,omitempty"`

	// The number of prices created during the time-range represented by the candlestick.
	Volume int32 `json:"volume,omitempty"`

	// A flag indicating if the candlestick is complete. A complete candlestick is one whose ending time is not in the future.
	Complete bool `json:"complete,omitempty"`
}

//return candlestick summary
func (c Candlestick) String() string {
	//output
	candle := fmt.Sprintf("Candlestick\n===========\nTime: %s\nVolume: %d\nComplete: %v\n", c.Time, c.Volume, c.Complete)

	if c.Bid != nil {
		candle += fmt.Sprintf("Bid: \n\tO: %s\n\tH: %s\n\tL: %s\n\tC: %s\n", c.Bid.O, c.Bid.H, c.Bid.L, c.Bid.C)
	}

	if c.Ask != nil {
		candle += fmt.Sprintf("Ask: \n\tO: %s\n\tH: %s\n\tL: %s\n\tC: %s\n", c.Ask.O, c.Ask.H, c.Ask.L, c.Ask.C)
	}

	if c.Mid != nil {
		candle += fmt.Sprintf("Mid: \n\tO: %s\n\tH: %s\n\tL: %s\n\tC: %s\n", c.Mid.O, c.Mid.H, c.Mid.L, c.Mid.C)
	}

	return candle
}

//CandlestickGetReqWrapper represents a GET req for candlesticks
type CandlestickGetReqWrapper struct {
	//PATH
	Instrument string //currency name - overrided by stip

	//QUERY
	Price             string //[BAM] Bid, Ask, Midpoint candles
	Granularity       string //i.e. S5, M5, H1, D, W, M
	Count             int
	From              string //RFC3339 time string
	To                string //RFC3339 time string
	Smooth            bool
	IncludeFirst      bool
	DailyAlignment    int    //hours of day for daily alignment (0-23)
	AlignmentTimezone string //default=America/New_York
	WeeklyAlignment   string //default=Friday

	//RESPONSE
	Print bool
}

func (cgrw CandlestickGetReqWrapper) getPath() string {
	return fmt.Sprintf(endpoints["getCandles"], cgrw.Instrument, cgrw.getEndodedQuery())
}

func (cgrw CandlestickGetReqWrapper) getEndodedQuery() string {
	//add data to query
	reqData := url.Values{}

	if cgrw.Count > 0 {
		reqData.Set("count", strconv.Itoa(cgrw.Count))
	}

	if len(cgrw.Price) > 0 {
		reqData.Set("price", cgrw.Price)
	}

	if len(cgrw.Granularity) > 0 {
		reqData.Set("granularity", cgrw.Granularity)
	}

	if len(cgrw.From) > 0 {
		reqData.Set("from", cgrw.From)
		reqData.Set("includeFirst", strconv.FormatBool(cgrw.IncludeFirst))
	}

	if len(cgrw.To) > 0 {
		reqData.Set("to", cgrw.To)
	}

	if cgrw.DailyAlignment > -1 {
		reqData.Set("dailyAlignment", strconv.Itoa(cgrw.DailyAlignment))
	}

	if len(cgrw.AlignmentTimezone) > 0 {
		reqData.Set("alignmentTimezone", cgrw.AlignmentTimezone)
	}

	if len(cgrw.WeeklyAlignment) > 0 {
		reqData.Set("weeklyAlignment", cgrw.WeeklyAlignment)
	}

	if cgrw.Smooth {
		reqData.Set("smooth", strconv.FormatBool(cgrw.Smooth))
	}

	//return encoded query string
	encodedReqData := reqData.Encode()
	if CheckLogLevel(3) {
		log.Printf("Returning encoded query string: %s", encodedReqData)
	}
	return encodedReqData
}

//PrintCandlesticks logs a summary of candlesticks
func PrintCandlesticks(cgrw CandlestickGetReqWrapper) (string, *ResponseGetCandles, error) {
	//log
	log.Println("v20: PrintCandlesticks...")

	//candlesticksResponse struct
	candlesticksResponse, _ := GetCandlesticksResponse(cgrw)

	//return error if account is nil
	if candlesticksResponse == nil || candlesticksResponse.Candles == nil {
		return "{}", nil, errors.New("candlesticks is nil")
	}

	//do something with candlesticksResponse

	//return body JSON as string
	jsonBytes, err := json.MarshalIndent(candlesticksResponse.Candles, "", "  ")
	checkPanic(err, "unable to marshal candlesticks to json!")

	//log candlesticks
	log.Println("v20: Candlesticks >>\n", string(jsonBytes))

	return string(jsonBytes), candlesticksResponse, nil
}

//GetCandlesticksResponse returns ResponseGetCandles
func GetCandlesticksResponse(cgrw CandlestickGetReqWrapper) (*ResponseGetCandles, error) {
	//get account response
	response, err := requestCandlesticksResponse(cgrw)
	checkPanic(err, "unable to get candlesticks response!")

	return response, err
}

//requestCandlesticksResponse returns a response from GET candlesticks
func requestCandlesticksResponse(cgrw CandlestickGetReqWrapper) (*ResponseGetCandles, error) {
	//define response type, req method, path w/params, and body
	response := &ResponseGetCandles{}
	method := "GET"

	//create path with encoded data
	path := cgrw.getPath()

	//DEBUG
	//log.Printf("requestCandlestsicks.. path: %s", path)

	//execute request and get response
	rawResponse, err := SendRequest(method, path, nil, nil, true)
	if err != nil {
		log.Printf("error executing new request >> method: %s, path: %s\n", method, path)
		return response, err
	}

	//ensure 200 status
	if rawResponse.Status != "200 OK" {
		return nil, errors.New("received status code " + rawResponse.Status)
	}

	//read response
	defer rawResponse.Body.Close()
	responseBytes, err := ioutil.ReadAll(rawResponse.Body)
	if err != nil {
		log.Println("unable to read response of http request!")
		return nil, err
	}

	//Unmarshal JSON response to object
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		log.Println("unable to marshal valid response to json!")
		return response, err
	}

	return response, err
}

//GoGetCandlesticksResponses takes slice of requests and executes concurrently
func GoGetCandlesticksResponses(reqs []CandlestickGetReqWrapper) ([]*ResponseGetCandles, error) {
	//log
	if CheckLogLevel(2) {
		log.Println("Inside 'fetchCandles'")
	}

	//output
	responses := []*ResponseGetCandles{}

	//channel for sending back candlestick slices
	reqsCount := len(reqs)
	sliceChan := make(chan *ResponseGetCandles, reqsCount)

	for _, r := range reqs {
		go goGoGetCandlesticksResponses(r, sliceChan)
	}

	//loop through all responses
	var response *ResponseGetCandles
	for i := 0; i < reqsCount; i++ {
		response = <-sliceChan
		if response == nil || response.Candles == nil || len(response.Candles) == 0 {
			log.Printf("%s candlesticks not retrieved; granularity: '%s'", response.Instrument, response.Granularity)
		} else {
			responses = append(responses, response)
		}
	}

	if len(responses) == 0 {
		return nil, errors.New("GoGetCandlesticksResponses: no responses retrieved")
	}

	return responses, nil
}

func goGoGetCandlesticksResponses(cgrw CandlestickGetReqWrapper, outChan chan *ResponseGetCandles) {
	var response *ResponseGetCandles
	var err error

	if cgrw.Print {
		_, response, err = PrintCandlesticks(cgrw)
	} else {
		response, err = GetCandlesticksResponse(cgrw)
	}

	if err != nil {
		log.Printf("goGetCandlesticksResponse: there was an error!\nmsg: %v", err)
	}

	//send response back to fetchCandles()
	outChan <- response
}

//GetMergedCompleteCandles merges to slices of candles by timestamp
func GetMergedCompleteCandles(baseCandles []Candlestick, proposedCandles []Candlestick) ([]Candlestick, []Candlestick) {
	//flag to tell if new candles detected
	var mergedCandles, newCandles []Candlestick
	var incompleteCandleCount int

	if len(proposedCandles) == 0 {
		return mergedCandles, newCandles
	}

	//if at least one candlestick exists in the baseCandles, assign its timestamp for comparison
	var lastCachedStartTime time.Time
	if len(baseCandles) > 0 {
		lastCachedStartTime, _ = time.Parse(time.RFC3339, baseCandles[len(baseCandles)-1].Time)
	}

	mergedCandles = baseCandles

	for _, c := range proposedCandles {
		if newCandleTime, _ := time.Parse(time.RFC3339, c.Time); c.Complete {
			if newCandleTime.After(lastCachedStartTime) {
				newCandles = append(newCandles, c)
				mergedCandles = append(mergedCandles, c)
			}
		} else {
			incompleteCandleCount++
		}
	}

	//log
	if CheckLogLevel(2) {
		log.Printf("GetUpdatedCandlesCacheMap:\n\tCandles added: %d\n\tIncomplete candles: %d\n", len(newCandles), incompleteCandleCount)
	}

	return mergedCandles, newCandles
}
