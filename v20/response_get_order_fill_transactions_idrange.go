package v20

//ResponseGetOrderFillTransactionsIDRange is the response struct for GET transactions/idrange (filtered by ORDER_FILL)
type ResponseGetOrderFillTransactionsIDRange struct {

	// The list of transactions found from query
	Transactions []OrderFillTransaction `json:"transactions,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}
