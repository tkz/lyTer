package v20

//ResponsePutTradeOrder represents response from successful PUT to Trade Order
type ResponsePutTradeOrder struct {
	TakeProfitOrderCancelTransaction *OrderCancelTransaction `json:"takeProfitOrderCancelTransaction,omitempty"`

	TakeProfitOrderTransaction *TakeProfitOrderTransaction `json:"takeProfitOrderTransaction,omitempty"`

	TakeProfitOrderFillTransaction *OrderFillTransaction `json:"takeProfitOrderFillTransaction,omitempty"`

	TakeProfitOrderCreatedCancelTransaction *OrderCancelTransaction `json:"takeProfitOrderCreatedCancelTransaction,omitempty"`

	StopLossOrderCancelTransaction *OrderCancelTransaction `json:"stopLossOrderCancelTransaction,omitempty"`

	StopLossOrderTransaction *StopLossOrderTransaction `json:"stopLossOrderTransaction,omitempty"`

	StopLossOrderFillTransaction *OrderFillTransaction `json:"stopLossOrderFillTransaction,omitempty"`

	StopLossOrderCreatedCancelTransaction *OrderCancelTransaction `json:"stopLossOrderCreatedCancelTransaction,omitempty"`

	TrailingStopLossOrderCancelTransaction *OrderCancelTransaction `json:"trailingStopLossOrderCancelTransaction,omitempty"`

	TrailingStopLossOrderTransaction *TrailingStopLossOrderTransaction `json:"trailingStopLossOrderTransaction,omitempty"`

	// The IDs of all Transactions that were created while satisfying the request.
	RelatedTransactionIDs []string `json:"relatedTransactionIDs,omitempty"`

	// The ID of the most recent Transaction created for the Account
	LastTransactionID string `json:"lastTransactionID,omitempty"`
}
