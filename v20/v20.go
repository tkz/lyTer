package v20

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

const (
	//PIP is a measurement of FX price movement; equal to 1/10,000 of the "numerator" currency
	PIP = 0.0001
)

var (
	//ENV
	env = map[string]string{
		"apiToken":             os.Getenv("API_TOKEN"),
		"basePath":             os.Getenv("BASE_PATH"),
		"accountID":            os.Getenv("ACCOUNT_ID"),
		"currencies":           os.Getenv("CURRENCIES"),
		"timeout":              os.Getenv("TIMEOUT"),
		"isDaylightSavings":    os.Getenv("IS_DAYLIGHT_SAVINGS"),
		"activeStrategyStruct": os.Getenv("ACTIVE_STRATEGY_STRUCT"),
		"activeStrategyConfig": os.Getenv("ACTIVE_STRATEGY_CONFIG"),
		"mailgunAuthHeader":    os.Getenv("MAILGUN_AUTH_HEADER"),
		"mailgunSender":        os.Getenv("MAILGUN_SENDER"),
		"mailgunRecipient":     os.Getenv("MAILGUN_RECIPIENT"),
		"mailgunURL":           os.Getenv("MAILGUN_URL"),
		"logLevel":             os.Getenv("LOG_LEVEL"),
	}

	endpoints = map[string]string{
		"getCandles":    "/instruments/%s/candles?%s",
		"getOpenPos":    "/accounts/%s/openPositions",
		"putClosePos":   "/accounts/%s/positions/%s/close",
		"postOrder":     "/accounts/%s/orders",
		"putOrder":      "/accounts/%s/orders/%s",
		"getTrade":      "/accounts/%s/trades/%s",
		"getOpenTrades": "/accounts/%s/openTrades",
		"getTxPages":    "/accounts/%s/transactions?%s",
		"getTxIDRange":  "/accounts/%s/transactions/idrange?%s",
	}

	//v20 client
	tr = &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    120 * time.Second,
		DisableCompression: true,
	}

	client = &http.Client{Transport: tr}

	defaultHeaders = map[string]string{
		"Connection":    "Keep-Alive",
		"Authorization": fmt.Sprint("Bearer ", os.Getenv("API_TOKEN")),
		"Content-Type":  "application/json",
	}
)

//LocalToCandleTime converts time from EST time to v20 timestamp
func LocalToCandleTime(localTime time.Time) string {
	//return time in UTC (native v20 time)
	location, _ := time.LoadLocation("UTC")
	localTime = localTime.In(location)

	//adjust for daylight savings if necessary
	isDaylightSavings, _ := strconv.ParseBool(GetEnv("isDaylightSavings"))
	if isDaylightSavings {
		localTime = localTime.Add(-time.Hour)
	}

	return localTime.Format(time.RFC3339)
}

//CandleToLocalTime converts time from v20 timestamp to EST time
func CandleToLocalTime(stamp string) time.Time {
	localTime, _ := time.Parse(time.RFC3339, stamp)

	//return time in EST (market local time)
	location, _ := time.LoadLocation("EST")
	localTime = localTime.In(location)

	//adjust for daylight savings if necessary
	isDaylightSavings, _ := strconv.ParseBool(GetEnv("isDaylightSavings"))
	if isDaylightSavings {
		localTime = localTime.Add(time.Hour)
	}

	return localTime
}

//GetEnv returns otherwise non-exported vars
func GetEnv(propName string) string {
	return env[propName]
}

//SendRequest returns a new request object with default headers
func SendRequest(method, path string, body io.Reader, headers map[string]string, isRelPath bool) (*http.Response, error) {
	//create request

	//add base path if relative url
	if isRelPath {
		path = env["basePath"] + path
	}

	//DEBUG
	//log.Printf("SendRequest >> path: %s", path)

	req, err := createRequest(method, path, body, headers)
	if err != nil {
		log.Println("unable to create new http request!")
		return nil, err
	}

	//do request
	rawResponse, err := client.Do(req)
	if err != nil {
		log.Println("unable to execute http request!")
		return nil, err
	}

	return rawResponse, err
}

//createRequest returns a new request object with default headers
func createRequest(method string, path string, body io.Reader, headers map[string]string) (*http.Request, error) {
	//create request
	req, err := http.NewRequest(method, path, body)
	if err != nil {
		log.Println("unable to create new http request!")
		return nil, err
	}

	//use default headers if headers not provided
	if headers == nil {
		for header, value := range defaultHeaders {
			req.Header.Add(header, value)
		}
	} else {
		for header, value := range headers {
			req.Header.Add(header, value)
		}
	}

	//DEBUG
	//log.Printf("create request:\n\treq: %v,\n\tmethod: %s,\n\tpath: %s", req, method, path)
	//log.Printf("v20: SendRequest:\nmethod: %s\npath: %s\nheaders: %v\n", method, path, req.Header)

	return req, err
}

/*
 * Util Funcs
 * ==========
 */

//checkPanic checks if there's an err and logs/panics if so
func checkPanic(err error, msg string) {
	if err != nil {
		log.Panicln(msg, "\n>>", err)
	}
}

//CheckLogLevel tests current env log level against checkLevel to determine if log should occur
func CheckLogLevel(checkLevel int) bool {
	//check env logLevel
	currentLevel, _ := strconv.Atoi(env["logLevel"])

	return currentLevel >= checkLevel
}
