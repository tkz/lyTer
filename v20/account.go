/*
 * OANDA v20 REST API
 *
 * The full OANDA v20 REST API Specification. This specification defines how to interact with v20 Accounts, Trades, Orders, Pricing and more.
 *
 * API version: 3.0.22
 * Contact: api@v20.com
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package v20

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
)

// Account includes full open Trade, open Position and pending Order representation.
type Account struct {

	// The Account's identifier
	ID string `json:"id,omitempty"`

	// Client-assigned alias for the Account. Only provided if the Account has an alias set
	Alias string `json:"alias,omitempty"`

	// The home currency of the Account
	Currency string `json:"currency,omitempty"`

	// The current balance of the Account.
	Balance string `json:"balance,omitempty"`

	// ID of the user that created the Account.
	CreatedByUserID int32 `json:"createdByUserID,omitempty"`

	// The date/time when the Account was created.
	CreatedTime string `json:"createdTime,omitempty"`

	// The current guaranteed Stop Loss Order mode of the Account.
	GuaranteedStopLossOrderMode string `json:"guaranteedStopLossOrderMode,omitempty"`

	// The total profit/loss realized over the lifetime of the Account.
	PL string `json:"pl,omitempty"`

	// The total realized profit/loss for the Account since it was last reset by the client.
	ResettablePL string `json:"resettablePL,omitempty"`

	// The date/time that the Account's resettablePL was last reset.
	ResettablePLTime string `json:"resettablePLTime,omitempty"`

	// The total amount of financing paid/collected over the lifetime of the Account.
	Financing string `json:"financing,omitempty"`

	// The total amount of commission paid over the lifetime of the Account.
	Commission string `json:"commission,omitempty"`

	// The total amount of fees charged over the lifetime of the Account for the execution of guaranteed Stop Loss Orders.
	GuaranteedExecutionFees string `json:"guaranteedExecutionFees,omitempty"`

	// Client-provided margin rate override for the Account. The effective margin rate of the Account is the lesser of this value and the OANDA margin rate for the Account's division. This value is only provided if a margin rate override exists for the Account.
	MarginRate string `json:"marginRate,omitempty"`

	// The date/time when the Account entered a margin call state. Only provided if the Account is in a margin call.
	MarginCallEnterTime string `json:"marginCallEnterTime,omitempty"`

	// The number of times that the Account's current margin call was extended.
	MarginCallExtensionCount int32 `json:"marginCallExtensionCount,omitempty"`

	// The date/time of the Account's last margin call extension.
	LastMarginCallExtensionTime string `json:"lastMarginCallExtensionTime,omitempty"`

	// The number of Trades currently open in the Account.
	OpenTradeCount int32 `json:"openTradeCount,omitempty"`

	// The number of Positions currently open in the Account.
	OpenPositionCount int32 `json:"openPositionCount,omitempty"`

	// The number of Orders currently pending in the Account.
	PendingOrderCount int32 `json:"pendingOrderCount,omitempty"`

	// Flag indicating that the Account has hedging enabled.
	HedgingEnabled bool `json:"hedgingEnabled,omitempty"`

	// The total unrealized profit/loss for all Trades currently open in the Account.
	UnrealizedPL string `json:"unrealizedPL,omitempty"`

	// The net asset value of the Account. Equal to Account balance + unrealizedPL.
	NAV string `json:"NAV,omitempty"`

	// Margin currently used for the Account.
	MarginUsed string `json:"marginUsed,omitempty"`

	// Margin available for Account currency.
	MarginAvailable string `json:"marginAvailable,omitempty"`

	// The value of the Account's open positions represented in the Account's home currency.
	PositionValue string `json:"positionValue,omitempty"`

	// The Account's margin closeout unrealized PL.
	MarginCloseoutUnrealizedPL string `json:"marginCloseoutUnrealizedPL,omitempty"`

	// The Account's margin closeout NAV.
	MarginCloseoutNAV string `json:"marginCloseoutNAV,omitempty"`

	// The Account's margin closeout margin used.
	MarginCloseoutMarginUsed string `json:"marginCloseoutMarginUsed,omitempty"`

	// The Account's margin closeout percentage. When this value is 1.0 or above the Account is in a margin closeout situation.
	MarginCloseoutPercent string `json:"marginCloseoutPercent,omitempty"`

	// The value of the Account's open positions as used for margin closeout calculations represented in the Account's home currency.
	MarginCloseoutPositionValue string `json:"marginCloseoutPositionValue,omitempty"`

	// The current WithdrawalLimit for the account which will be zero or a positive value indicating how much can be withdrawn from the account.
	WithdrawalLimit string `json:"withdrawalLimit,omitempty"`

	// The Account's margin call margin used.
	MarginCallMarginUsed string `json:"marginCallMarginUsed,omitempty"`

	// The Account's margin call percentage. When this value is 1.0 or above the Account is in a margin call situation.
	MarginCallPercent string `json:"marginCallPercent,omitempty"`

	// The ID of the last Transaction created for the Account.
	LastTransactionID string `json:"lastTransactionID,omitempty"`

	// The details of the Trades currently open in the Account.
	Trades []TradeSummary `json:"trades,omitempty"`

	// The details all Account Positions.
	Positions []Position `json:"positions,omitempty"`

	// The details of the Orders currently pending in the Account.
	Orders []Order `json:"orders,omitempty"`
}

func (acct *Account) String() string {
	//only print last 3 digits of account number
	accountID := acct.ID
	acct.ID = "***" + acct.ID[len(accountID)-3:]

	//return body JSON as string
	jsonBytes, err := json.MarshalIndent(acct, "", "  ")
	checkPanic(err, "unable to marshal account to json!")

	//return account summary
	return fmt.Sprintf("\nv20: Account Summary\n====================\n%s\n", string(jsonBytes))
}

//GetAccount returns JSON representation of Account
func GetAccount() (*Account, error) {
	//get account response
	response, err := requestAccount()
	checkPanic(err, "unable to get account response!")

	//return error if account is nil
	if response.Account == nil {
		return nil, errors.New("account is nil")
	}

	return response.Account, nil
}

//requestAccount returns a response from GET account
func requestAccount() (*ResponseGetAccount, error) {
	//define response type, req method, path w/params, and body
	response := &ResponseGetAccount{}
	method := "GET"
	path := "/accounts/" + env["accountID"]

	//execute request and get response
	rawResponse, err := SendRequest(method, path, nil, nil, true)
	if err != nil {
		log.Printf("error executing new request >> method: %s, path: %s\n", method, path)
		return response, err
	}

	//ensure 200 status
	if rawResponse.Status != "200 OK" {
		return nil, errors.New("received status code " + rawResponse.Status)
	}

	//read response
	defer rawResponse.Body.Close()
	responseBytes, err := ioutil.ReadAll(rawResponse.Body)
	if err != nil {
		log.Println("unable to read response of http request!")
		return nil, err
	}

	//Unmarshal JSON response to object
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		log.Println("unable to marshal valid response to json!")
		return response, err
	}

	return response, err
}
