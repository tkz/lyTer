/*
 * OANDA oanda REST API
 *
 * The full OANDA oanda REST API Specification. This specification defines how to interact with oanda Accounts, Trades, Orders, Pricing and more.
 *
 * API version: 3.0.22
 * Contact: api@v20.com
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package v20

import (
	"fmt"
)

// A ClientExtensions object allows a client to attach a clientID, tag and comment to Orders and Trades in their Account.  Do not set, modify, or delete this field if your account is associated with MT4.
type ClientExtensions struct {

	// The Client ID of the Order/Trade
	ID string `json:"id,omitempty"`

	// A tag associated with the Order/Trade
	Tag string `json:"tag,omitempty"`

	// A comment associated with the Order/Trade
	Comment string `json:"comment,omitempty"`
}

func (ce *ClientExtensions) String() string {
	return fmt.Sprintf("ClientExtensions\n===============\nID: %s\nTag: %s\nComment: %s", ce.ID, ce.Tag, ce.Comment)
}
